﻿namespace ContainerOrchestrator.Docker.Services.Container.Logs;

public class LogsArguments
{
    public string Since { get; set; }
}
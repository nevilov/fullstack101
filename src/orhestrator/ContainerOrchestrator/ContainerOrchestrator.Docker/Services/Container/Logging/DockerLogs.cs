﻿using ContainerOrchestrator.Docker.Helpers.Builder;
using ContainerOrchestrator.Docker.Services.Container.Logs;

namespace ContainerOrchestrator.Docker.Services.Container.Logging;

public class DockerLogs : Docker
{
    public async Task<string> Logs(string containerName, LogsArguments args)
    {
        var builtQuery = ArgumentBuilder.AppendArguments(args);
        var query = $"logs {builtQuery} {containerName}";
        var result = await Run(query);

        return result.FullMessage;
    }
}
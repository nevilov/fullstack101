﻿namespace ContainerOrchestrator.Docker.Services.Container.Run;

public class RunResponse
{
    public RunResponse(string? containerName, string? error)
    {
        ContainerName = containerName;
        Error = error;
    }

    public string? ContainerName { get; set; }
    public string? Error { get; set; }
}
﻿using ContainerOrchestrator.Docker.Helpers.Builder;
using ContainerOrchestrator.Docker.Helpers.Operation;
using ContainerOrchestrator.Docker.Services.Container.Run;

namespace ContainerOrchestrator.Docker.Commands.Run;

public class DockerRun : Docker
{
    public async Task<OperationResponse> Run(RunArguments arguments, string imageName)
    {
        var builtQuery = ArgumentBuilder.AppendArguments(arguments);
        var query = $"run {builtQuery} {imageName}";
        var result = await Run(query);

        return result;
    }
}
﻿namespace ContainerOrchestrator.Docker.Services.Container.Run;

public class RunArguments
{
    public bool Interactive { get; set; }
    public string? Publish { get; set; }
    public bool Detach { get; set; }
    public string Name { get; set; } = null!;
    public string[] Env { get; set; } = null!;
    public string Network { get; set; } = null!;

    public string? Expose => !string.IsNullOrEmpty(Publish) ? Publish.Split(":")[0] : null;
}
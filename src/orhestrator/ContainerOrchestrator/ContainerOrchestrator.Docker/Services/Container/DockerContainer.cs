﻿using ContainerOrchestrator.Docker.Commands.Run;
using ContainerOrchestrator.Docker.Services.Container.Logging;
using ContainerOrchestrator.Docker.Services.Container.Rm;

namespace ContainerOrchestrator.Docker.Services.Container;

public class DockerContainer
{
    public DockerContainer(DockerRun dockerRun, DockerRm dockerRm, DockerLogs dockerLogs)
    {
        DockerRun = dockerRun;
        DockerRm = dockerRm;
        DockerLogs = dockerLogs;
    }

    public DockerRun DockerRun { get; set; }
    public DockerRm DockerRm { get; set; }
    public DockerLogs DockerLogs { get; set; }
}
﻿using ContainerOrchestrator.Docker.Helpers.Operation;

namespace ContainerOrchestrator.Docker.Services.Container.Rm;

public class DockerRm : Docker
{
    public async Task<OperationResponse> Remove(string containerName)
    {
        var query = $"rm {containerName} --force";
        return await Run(query);
    }
}
﻿using ContainerOrchestrator.Docker.Helpers.Operation;

namespace ContainerOrchestrator.Docker.Services.Image.Rmi;

public class DockerRmi : Docker
{
    public async Task<OperationResponse> Remove(string imageName)
    {
        var command = $"rmi -f {imageName}";
        return await Run(command);
    }
}
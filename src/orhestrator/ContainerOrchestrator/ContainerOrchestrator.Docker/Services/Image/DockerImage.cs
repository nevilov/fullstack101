﻿using ContainerOrchestrator.Docker.Services.Image.Build;
using ContainerOrchestrator.Docker.Services.Image.Rmi;

namespace ContainerOrchestrator.Docker.Services.Image;

public class DockerImage
{
    public readonly DockerBuild DockerBuild;
    public readonly DockerRmi DockerRmi;

    public DockerImage(DockerBuild dockerBuild, DockerRmi dockerRmi)
    {
        DockerBuild = dockerBuild;
        DockerRmi = dockerRmi;
    }
}
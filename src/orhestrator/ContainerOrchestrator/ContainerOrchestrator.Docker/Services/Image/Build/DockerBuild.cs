﻿using ContainerOrchestrator.Docker.Helpers.Operation;

namespace ContainerOrchestrator.Docker.Services.Image.Build;

public class DockerBuild : Docker
{
    public async Task<OperationResponse> Build(string dockerImagePath, string args)
    {
        var command = $"build -f {dockerImagePath} {args} .";
        return await Run(command);
    }
}
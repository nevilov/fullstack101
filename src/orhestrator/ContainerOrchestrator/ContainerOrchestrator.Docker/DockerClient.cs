﻿using ContainerOrchestrator.Docker.Services.Container;
using ContainerOrchestrator.Docker.Services.Image;

namespace ContainerOrchestrator.Docker;

public class DockerClient : IDockerClient
{
    public DockerClient(DockerContainer container, DockerImage image)
    {
        Container = container;
        Image = image;
    }

    public DockerContainer Container { get; set; }
    public DockerImage Image { get; set; }
}
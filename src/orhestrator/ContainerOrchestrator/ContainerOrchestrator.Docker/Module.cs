﻿using ContainerOrchestrator.Docker.Commands.Run;
using ContainerOrchestrator.Docker.Services.Container;
using ContainerOrchestrator.Docker.Services.Container.Logging;
using ContainerOrchestrator.Docker.Services.Container.Rm;
using ContainerOrchestrator.Docker.Services.Image;
using ContainerOrchestrator.Docker.Services.Image.Build;
using ContainerOrchestrator.Docker.Services.Image.Rmi;
using Microsoft.Extensions.DependencyInjection;

namespace ContainerOrchestrator.Docker;

public static class Module
{
    public static IServiceCollection AddDockerModule(this IServiceCollection services)
    {
        services.AddScoped<IDockerClient, DockerClient>();

        services.AddScoped<DockerContainer>().AddScoped<DockerImage>();

        services
            .AddScoped<DockerRun>()
            .AddScoped<DockerRm>()
            .AddScoped<DockerLogs>();

        services.AddScoped<DockerBuild>()
            .AddScoped<DockerRmi>();
        return services;
    }
}
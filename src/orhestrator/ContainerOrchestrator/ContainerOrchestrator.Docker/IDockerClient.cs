﻿using ContainerOrchestrator.Docker.Services.Container;
using ContainerOrchestrator.Docker.Services.Image;

namespace ContainerOrchestrator.Docker;

public interface IDockerClient
{
    public DockerContainer Container { get; set; }
    public DockerImage Image { get; set; }
}
﻿using System.Reflection;

namespace ContainerOrchestrator.Docker.Helpers.Builder;

public class ArgumentBuilder
{
    public static string AppendArguments<TSettings>(TSettings settings)
    {
        List<DockerArgument> arguments = new();
        foreach (var property in typeof(TSettings).GetProperties(BindingFlags.Public | BindingFlags.Instance))
        {
            var argument = GetArgumentFromProperty(property, settings);
            if (argument is not null && argument.Any())
            {
                arguments.AddRange(argument);
            }
        }

        var resultQuery = arguments.Select(argument => $"{argument.Key ?? ""} {argument.Value ?? ""}");
        return string.Join(" ", resultQuery);
    }

    private static IEnumerable<DockerArgument>? GetArgumentFromProperty<TSettings>(PropertyInfo property,
        TSettings settings)
    {
        if (property.PropertyType == typeof(bool) || property.PropertyType == typeof(bool?))
        {
            return new List<DockerArgument>()
            {
                new DockerArgument(null,
                    GetArgumentFromBoolProperty(property, (bool)(property.GetValue(settings) ?? false)),
                    DockerArgumentQuoting.Unquoted)

            };
        }
        if (property.PropertyType == typeof(int?) || property.PropertyType == typeof(int))
        {
            return new List<DockerArgument>()
            {
                new DockerArgument(GetArgumentFromNullableIntProperty(property, (int?)property.GetValue(settings)),
                    ((int?)property.GetValue(settings)).HasValue
                        ? $"{(((int?)property.GetValue(settings))!).Value}"
                        : null,
                    DockerArgumentQuoting.Unquoted)
            };
        }
        if (property.PropertyType == typeof(string))
        {
            return new List<DockerArgument>()
            {
                GetArgumentFromStringProperty(property, (string)property.GetValue(settings)!, false)
            };
        }

        if (property.PropertyType == typeof(string[]))
        {
            List<DockerArgument> arguments = new();
            return GetArgumentFromStringArrayProperty(property, (string[])property.GetValue(settings));
        }
        return null;
    }
    
    public static IEnumerable<DockerArgument?> GetArgumentFromStringArrayProperty(PropertyInfo property, string[] values)
    {
        
        if (values != null)
        {
            List<DockerArgument> arguments = new();
            foreach (string value in values)
            {
                arguments.Add(GetArgumentFromStringProperty(property, value, false));
            }

            return arguments;
        }

        return null;
    }

    private static string? GetArgumentFromBoolProperty(PropertyInfo property, bool? value)
    {
        return value.GetValueOrDefault() ? $"--{GetPropertyName(property.Name)}" : null;
    }

    private static string? GetArgumentFromNullableIntProperty(PropertyInfo property, int? value)
    {
        return value.HasValue ? $"--{GetPropertyName(property.Name)}" : null;
    }

    private static DockerArgument? GetArgumentFromStringProperty(PropertyInfo property, string? value, bool isSecret)
    {
        if (!string.IsNullOrEmpty(value))
        {
            return new DockerArgument($"--{GetPropertyName(property.Name)}", value,
                isSecret ? DockerArgumentQuoting.QuotedSecret : DockerArgumentQuoting.Quoted);
        }

        return null;
    }

    /// <summary>
    /// Converts property name to docker arguments format
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    /// <example>NoForce -> no-force</example>
    private static string GetPropertyName(string name)
    {
        string result = null;
        if (!string.IsNullOrEmpty(name))
        {
            result = name.Substring(0, 1).ToLower();
            if (name.Length > 1)
            {
                foreach (char c in name.Substring(1))
                {
                    if (char.IsUpper(c))
                    {
                        result += "-" + char.ToLower(c);
                    }
                    else
                    {
                        result += c;
                    }
                }
            }
        }

        return result;
    }
}
﻿namespace ContainerOrchestrator.Docker.Helpers.Operation;

public class OperationSetting
{
    public string Command { get; set; }
    public string Arguments { get; set; }
    public bool Background { get; set; } = true;
} 
﻿namespace ContainerOrchestrator.Docker.Helpers.Operation;

public class OperationResponse
{
    public string? OutputMessage { get; set; }
    public string? ErrorMessage { get; set; }
    public string FullMessage => $"{OutputMessage} / {ErrorMessage}";
    public bool IsFinishedSuccessfully { get; set; }
}
﻿using System.Diagnostics;

namespace ContainerOrchestrator.Docker.Helpers.Operation;

public class OperationHelper
{
    public async Task<OperationResponse> Run(OperationSetting setting)
    {
        var processInfo = new ProcessStartInfo(setting.Command, setting.Arguments)
        {
            CreateNoWindow = true,
            UseShellExecute = false,
            RedirectStandardOutput = true,
            RedirectStandardError = true
        };

        using var process = new Process();
        process.StartInfo = processInfo;

        var response = new OperationResponse();
        process.OutputDataReceived += (_, e) =>
        {
            if (string.IsNullOrWhiteSpace(e.Data))
            {
                return;
            }
            
            response.OutputMessage += $"{e.Data}\n";
        };
        process.ErrorDataReceived += (_, e) =>
        {
            if (string.IsNullOrWhiteSpace(e.Data))
            {
                return;
            }
            
            response.ErrorMessage += $"{e.Data}\n";
        };
        
        process.Start();
        process.BeginOutputReadLine();
        process.BeginErrorReadLine();
        
        if (setting.Background)
        {
            try
            {
                using var cts = new CancellationTokenSource(50000);
                await process.WaitForExitAsync(cts.Token);
                if (!process.HasExited)
                {
                    process.Kill();
                }
            }
            catch (Exception exception)
            {
                Console.Write(exception.Message);
            }
        }
        
        response.IsFinishedSuccessfully =  process.ExitCode == 0;
        return response;
    }
}
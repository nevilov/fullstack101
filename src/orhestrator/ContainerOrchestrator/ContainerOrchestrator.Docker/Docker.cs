﻿using ContainerOrchestrator.Docker.Helpers.Operation;

namespace ContainerOrchestrator.Docker;

public abstract class Docker
{
    protected async Task<OperationResponse> Run(string arguments)
    {
        OperationHelper operation = new();
        return await operation.Run(new OperationSetting()
        {
            Command = "docker",
            Arguments = arguments,
        });
    }
}
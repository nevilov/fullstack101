using ContainerOrchestrator.Api;
using ContainerOrchestrator.BackgroundServices;
using ContainerOrchestrator.Core;
using ContainerOrchestrator.Infrastructure;
using Serilog;

var builder = WebApplication.CreateBuilder(args);
builder.Host.UseSerilog((hostingContext, loggerConfiguration) =>
{
    loggerConfiguration
        .ReadFrom.Configuration(hostingContext.Configuration)
        .WriteTo.Console();
});

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCoreModule(builder.Configuration);
builder.Services.AddInfrastructureModule(builder.Configuration);
builder.Services.AddBackgroundServicesModule();
builder.Services.AddHostModule();

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.UseAuthorization();

app.UseCors(options =>
{
    options
        .WithOrigins("http://fullstack101.ru")
        .AllowAnyHeader()
        .AllowAnyMethod()
        .AllowCredentials();
});

app.MapControllers();

app.Run();
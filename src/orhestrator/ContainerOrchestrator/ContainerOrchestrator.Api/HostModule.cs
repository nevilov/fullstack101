﻿using Serilog;

namespace ContainerOrchestrator.Api;

public static class HostModule
{
    public static IServiceCollection AddHostModule(this IServiceCollection serviceCollection)
    {
        Serilog.Debugging.SelfLog.Enable(Console.Error);

        return serviceCollection;
    }
}
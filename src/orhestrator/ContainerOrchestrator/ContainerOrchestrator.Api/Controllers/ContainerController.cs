﻿using ContainerOrchestrator.Core.Dtos;
using ContainerOrchestrator.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace ContainerOrchestrator.Api.Controllers;

[ApiController]
[Route("api/container")]
public class ContainerController : ControllerBase
{
    private readonly ContainerService _containerService;

    public ContainerController(ContainerService containerService)
    {
        _containerService = containerService;
    }

    [HttpGet]
    public async Task<IActionResult> Get()
    {
        return Ok(await _containerService.Get());
    }

    [HttpGet("project/{projectId:guid}")]
    public async Task<IActionResult> GetContainerByProjectId(Guid projectId, [FromQuery] GetContainerRequestDto dto)
    {
        return Ok(await _containerService.Get(projectId, dto));
    }

    [HttpPost]
    public async Task<IActionResult> Create(CreateContainerRequestDto dto)
    {
        await _containerService.Create(dto);
        return Ok();
    }

    [HttpDelete("project/{projectId:guid}")]
    public async Task<IActionResult> Delete(Guid projectId)
    {
        await _containerService.Delete(projectId);
        return Ok();
    }
}
﻿namespace ContainerOrchestrator.Tests;

public class DockerTests
{
    [Fact]
    public void DockerRunOperation_Success()
    {
        Assert.Equal(1,1);
    }

    [Fact]
    public void DockerLogsOperation_Success()
    {
        Assert.Equal(1,1);
    }

    [Fact]
    public void DockerRmOperation_Success()
    {
        Assert.Equal(1,1);
    }

    [Fact]
    public void DockerImageBuild_Success()
    {
        Assert.Equal(1,1);
    }
}
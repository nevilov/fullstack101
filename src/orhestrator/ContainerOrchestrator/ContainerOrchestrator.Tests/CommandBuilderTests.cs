﻿using ContainerOrchestrator.Docker.Helpers.Builder;
using ContainerOrchestrator.Docker.Services.Container.Logs;
using ContainerOrchestrator.Docker.Services.Container.Run;

namespace ContainerOrchestrator.Tests;

public class CommandBuilderTests
{
    [Fact]
    public void AppendArgumentLogsSuccess()
    {
        var logs = new LogsArguments()
        {
            Since = "15m"
        };

        var result = ArgumentBuilder.AppendArguments(logs);
        
        Assert.Equal("--since 15m", result);
    }
    
    [Fact]
    public void AppendArgumentRunSuccess()
    {
        //Arrange
        var item = new RunArguments()
        {
            Interactive = true,
            Publish = "5000:5000",
            Name = "test"
        };

        //Act
        var result = ArgumentBuilder.AppendArguments(item);
        
        //Assert
        Assert.Equal(" --interactive --publish 5000:5000   --name test --expose 5000", result);
    }

    [Fact]
    public void AppendArgumentsComplexSuccess()
    {
        //Arrange
        var item = new RunArguments()
        {
            Interactive = true,
            Publish = "5000:5000",
            Name = "test",
            Env = new []
            {
                "VIRTUAL_HOST=te.fullstack101.ru",
                "VIRTUAL_PORT=4200"
            },
            Network = "nginx_default",
        };

        //Act
        var result = ArgumentBuilder.AppendArguments(item);
        
        //Assert
        Assert.Equal(" --interactive --publish 5000:5000   --name test --env VIRTUAL_HOST=te.fullstack101.ru --env VIRTUAL_PORT=4200 --network nginx_default --expose 5000", result);
    }
}
﻿using Amazon.Runtime;
using Amazon.S3;
using ContainerOrchestrator.Core.Abstractions;
using ContainerOrchestrator.Core.Configs;
using ContainerOrchestrator.Core.Services;
using ContainerOrchestrator.Docker;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ContainerOrchestrator.Core;

public static class Module
{
    public static IServiceCollection AddCoreModule(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddScoped<ContainerService>();
        services.AddDockerModule();
        
        AddS3(services, configuration);
        
        return services;
    }
    
    private static IServiceCollection AddS3(IServiceCollection services, IConfiguration configuration)
    {
        const string s3SectionName = "S3";
        var options = configuration.GetAWSOptions(s3SectionName);
            
        options.Credentials =
            new BasicAWSCredentials(
                configuration[$"{s3SectionName}:AWSAccessKey"],
                configuration[$"{s3SectionName}:AWSSecretKey"]
            );
            
        services.AddAWSService<IAmazonS3>(options);
        services.AddScoped<IS3Service, S3Service>();

        var s3Config = configuration
            .GetRequiredSection(s3SectionName)
            .Get<AwsS3Config>()!;
        services.AddSingleton(s3Config);
            
        return services;
    }
}
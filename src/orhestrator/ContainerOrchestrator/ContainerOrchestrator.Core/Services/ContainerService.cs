﻿using System.Text;
using ContainerOrchestrator.Core.Abstractions;
using ContainerOrchestrator.Core.Dtos;
using ContainerOrchestrator.Core.Helpers;
using ContainerOrchestrator.Docker;
using ContainerOrchestrator.Docker.Helpers.Operation;
using ContainerOrchestrator.Docker.Services.Container.Logs;
using ContainerOrchestrator.Docker.Services.Container.Run;
using ContainerOrchestrator.Infrastructure;
using ContainerOrchestrator.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace ContainerOrchestrator.Core.Services;

public class ContainerService
{
    private readonly OrchestratorDbContext _dbContext;
    private readonly IS3Service _s3Service;
    private readonly IDockerClient _dockerClient;
    private readonly ILogger _logger;

    public ContainerService(OrchestratorDbContext dbContext, IDockerClient dockerClient, IS3Service s3Service,
        ILogger logger)
    {
        _dbContext = dbContext;
        _dockerClient = dockerClient;
        _s3Service = s3Service;
        _logger = logger;
    }

    public async Task<IEnumerable<Container>> Get()
    {
        return await _dbContext.Containers.Include(x => x.Image).ToListAsync();
    }

    public async Task<ContainerDto?> Get(Guid projectId, GetContainerRequestDto dto)
    {
        var container = await _dbContext.Containers.FirstOrDefaultAsync(x => x.ProjectId == projectId);

        if (container is null)
        {
            return null;
        }

        var containerDto = new ContainerDto()
        {
            Id = container.Id,
            BuildOutput = container.BuildOutput,
            LaunchedAt = container.LaunchedAt,
            LifetimeMinute = container.LifetimeMinute,
            Name = container.Name,
            Port = container.Port,
            Status = container.Status,
            ProjectId = container.ProjectId,
            IsInProgress = container.InSetupProgress
        };

        if (container?.Status is ContainerStatus.Running)
        {
            containerDto.LastContainerLogs = await _dockerClient.Container.DockerLogs.Logs(container.Name, new LogsArguments()
            {
                Since = $"{dto.LogsSinceMinutes}m"
            });
        }

        return containerDto;
    }

    private async Task<OperationResponse> CreateImage(Guid projectId, Image image, string command, string args, string port)
    {
        var containerPort = int.Parse(port.Split(":")[0]);
        var s3Link = $"{_s3Service.GetUrl().TrimEnd('/')}/projects/{projectId}.zip";
        var buildArgs = CommandBuilder.BuildArgs(s3Link, projectId.ToString(), containerPort);
        var dockerfilePath = await DockerImageHelper.PrepareImage(command, args, image);
        
        var dockerBuildResponse = await _dockerClient.Image.DockerBuild.Build(dockerfilePath, buildArgs);
        File.Delete(dockerfilePath);

        return dockerBuildResponse;
    }

    public async Task Create(CreateContainerRequestDto dto)
    {
        var image = await _dbContext.Images.FirstOrDefaultAsync(x => x.Name == dto.ImageName);

        if (image is null)
        {
            throw new NotSupportedException($"Image {dto.ImageName} is not supported");
        }

        await DeleteExistingContainer(dto.ProjectId);

        var container = new Container()
        {
            Name = dto.ProjectId.ToString(),
            ProjectId = dto.ProjectId,
            Port = dto.Port,
            Image = image,
            InSetupProgress = true,
            CreatedAt = DateTime.UtcNow,
            Status = ContainerStatus.InQueue,
            LifetimeMinute = 15 //TODO: Think about privilege
        };
        await _dbContext.Containers.AddAsync(container);
        await _dbContext.SaveChangesAsync();

        _logger.Information("Creating image for projectId {projectId}", dto.ProjectId);

        var dockerBuildResponse = await CreateImage(dto.ProjectId, image, dto.Command, dto.Arguments, dto.Port);
        container.BuildOutput = dockerBuildResponse.FullMessage;

        if (!dockerBuildResponse.IsFinishedSuccessfully)
        {
            _logger.Error("Creating image for projectId {projectId} failed!. Message = {message}", dto.ProjectId,
                dockerBuildResponse.FullMessage);
            await UpdateStatus(container, ContainerStatus.ImageCreationFailed, false);
            return;
        }

        _logger.Information("Creating image for projectId {projectId} success!. Message = {message}", dto.ProjectId,
            dockerBuildResponse.FullMessage);
        await UpdateStatus(container, ContainerStatus.ContainerCreationInQueue, true);

        var portNumber = dto.Port.Split(":")[0];
        var containerArguments = new RunArguments()
        {
            Interactive = dto.Interactive,
            Detach = dto.Background,
            Name = dto.Name,
            Publish = dto.Port,
            Env = new string[]
            {
                $"VIRTUAL_PORT={portNumber}",
                $"VIRTUAL_HOST={dto.Name}.js3.ru"
            },
            Network = "nginx_default",
        };
        var response = await _dockerClient.Container.DockerRun.Run(containerArguments, dto.ProjectId.ToString());
        container.BuildOutput += $"\n{response.FullMessage}";
        if (!response.IsFinishedSuccessfully)
        {
            await UpdateStatus(container, ContainerStatus.ContainerCreationFailed, false);
            return;
        }

        container.LaunchedAt = DateTime.UtcNow;
        container.BuildOutput += $"\nВаше приложение успешно запущено и развернуто! {dto.Name}.js3.ru";
        await UpdateStatus(container, ContainerStatus.Running, false);
    }

    private async Task DeleteExistingContainer(Guid projectId)
    {
        var containersToBeDeleted = await _dbContext.Containers
            .Where(x => x.ProjectId == projectId && x.Status != ContainerStatus.Killed && x.Status != ContainerStatus.Destroyed)
            .ToListAsync();

        foreach (var container in containersToBeDeleted)
        {
            var result = await _dockerClient.Container.DockerRm.Remove(container.Name);
            if (result.IsFinishedSuccessfully)
            {
                container.Status = ContainerStatus.Killed;
            }
        }

        await _dbContext.SaveChangesAsync();
    }

    private async Task UpdateStatus(Container container, ContainerStatus status, bool isInProgress)
    {
        container.Status = status;
        container.InSetupProgress = isInProgress;
        await _dbContext.SaveChangesAsync();
    }

    public async Task Delete(Guid projectId)
    {
        var container = await _dbContext.Containers
            .FirstOrDefaultAsync(x => x.ProjectId == projectId);

        if (container is null)
        {
            throw new Exception("An container was not found");
        }

        await _dockerClient.Container.DockerRm.Remove(container.Name);
    }
}
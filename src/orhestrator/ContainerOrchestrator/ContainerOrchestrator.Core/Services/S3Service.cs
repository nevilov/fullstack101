using Amazon.S3;
using Amazon.S3.Model;
using ContainerOrchestrator.Core.Abstractions;
using ContainerOrchestrator.Core.Configs;
using ContainerOrchestrator.Core.Dtos;

namespace ContainerOrchestrator.Core.Services
{
    public class S3Service : IS3Service
    {
        private readonly IAmazonS3 _s3;
        private readonly AwsS3Config _awsConfig;
        public S3Service(IAmazonS3 s3, AwsS3Config awsConfig)
        {
            _s3 = s3;
            _awsConfig = awsConfig;
        }

        public string GetObjectUrl(string key) => $"{_awsConfig.CustomServiceUrl.Trim('/')}/{key}";


        public async Task CreateObject(S3ObjectRequest obj)
        {
            await _s3.PutObjectAsync(new PutObjectRequest
            {
                Key = obj.Key,
                BucketName = _awsConfig.BucketName,
                CannedACL = S3CannedACL.PublicRead,
                ContentBody = obj.Content,
            });
        }

        public string GetUrl() => _awsConfig.CustomServiceUrl;
        public async Task<IList<string>> ListBucketContent(string prefix)
        {
            var request = new ListObjectsV2Request()
            {
                BucketName = _awsConfig.BucketName,
                Prefix = prefix
            };

            var listObjects = await _s3.ListObjectsV2Async(request);
            return listObjects.S3Objects.Select(x => x.Key).ToList();
        }
    }
}
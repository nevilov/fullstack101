using ContainerOrchestrator.Core.Dtos;

namespace ContainerOrchestrator.Core.Abstractions
{
    public interface IS3Service
    {
        Task<IList<string>> ListBucketContent(string prefix);
        Task CreateObject(S3ObjectRequest obj);

        string GetObjectUrl(string key);
        string GetUrl();
    }
}
﻿namespace ContainerOrchestrator.Core.Dtos;

public class GetContainerRequestDto
{
    public int LogsSinceMinutes { get; set; }
}
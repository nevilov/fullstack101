﻿namespace ContainerOrchestrator.Core.Dtos;

public class CreateContainerRequestDto
{
    public bool Interactive { get; set; }
    public string? Port { get; set; }
    public bool Background { get; set; }
    public string Name { get; set; } = null!;
    public string ImageName { get; set; } = null!;
    public string Command { get; set; }
    
    public string Arguments { get; set; }
    public Guid ProjectId { get; set; }
    public bool OccupyPort { get; set; }
}
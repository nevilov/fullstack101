﻿using ContainerOrchestrator.Infrastructure.Entities;

namespace ContainerOrchestrator.Core.Dtos;

public class ContainerDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string? Port { get; set; }
    public int LifetimeMinute { get; set; }
    public DateTime? LaunchedAt { get; set; }
    public string? BuildOutput { get; set; }
    public ContainerStatus Status { get; set; }
    public Guid ProjectId { get; set; }
    public bool IsInProgress { get; set; }
    
    public string? LastContainerLogs { get; set; }
}
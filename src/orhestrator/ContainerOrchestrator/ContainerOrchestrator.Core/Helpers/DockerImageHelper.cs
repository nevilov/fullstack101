﻿using System.Text;
using ContainerOrchestrator.Core.Dtos;
using ContainerOrchestrator.Infrastructure.Entities;

namespace ContainerOrchestrator.Core.Helpers;

public class DockerImageHelper
{
    public static string ReplaceCustomArgs(string original, string command, string args)
    {
        return original
            .Replace("<CUSTOM_COMMAND>", command)
            .Replace("<CUSTOM_ARGS>", args);
    }
    
    public static async Task<string> PrepareImage(string command, string args, Image image)
    {
        var templateImage = ReplaceCustomArgs(image.TemplateDockerfile, command, args);
        var dockerfilePath = Path.GetTempFileName();
        await File.WriteAllTextAsync(dockerfilePath, templateImage, Encoding.UTF8);
        
        return dockerfilePath;
    }
}
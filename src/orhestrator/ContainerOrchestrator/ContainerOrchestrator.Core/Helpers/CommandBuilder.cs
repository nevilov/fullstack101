﻿namespace ContainerOrchestrator.Core.Helpers;

public class CommandBuilder
{
    public static string BuildArgs(string s3Link, string projectId, int exposePort)
    {
        var buildArgs = BuildArgs(new Dictionary<string, string>()
        {
            { "FILE_KEY", projectId },
            { "S3_LINK", s3Link },
            { "EXPOSE_PORT", exposePort.ToString() },
        });

        return $"{buildArgs} -t {projectId}";
    }

    public static string BuildArgs(IDictionary<string, string> args)
    {
        return string.Join(" ", args.Select(x => $"--build-arg {x.Key}={x.Value}"));
    }
}
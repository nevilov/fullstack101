﻿using ContainerOrchestrator.Core.Services;
using ContainerOrchestrator.Docker;
using ContainerOrchestrator.Infrastructure;
using ContainerOrchestrator.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;

namespace ContainerOrchestrator.BackgroundServices.Jobs.Handlers;

public class ContainerManagerHandler
{
    private readonly ContainerService _containerService;
    private readonly OrchestratorDbContext _dbContext;
    private readonly IDockerClient _dockerClient;
    
    public ContainerManagerHandler(ContainerService containerService, OrchestratorDbContext dbContext, IDockerClient dockerClient)
    {
        _containerService = containerService;
        _dbContext = dbContext;
        _dockerClient = dockerClient;
    }

    public async Task Handle()
    {
        var containersToBeDeleted = await _dbContext.Containers
            .Where(x => x.LaunchedAt.HasValue && x.LaunchedAt.Value.AddMinutes(x.LifetimeMinute) <= DateTime.UtcNow)
            .Where(x => x.Status != ContainerStatus.Destroyed)
            .ToListAsync();

        foreach (var container in containersToBeDeleted)
        {
            var containerRemovalResult = await _dockerClient.Container.DockerRm.Remove(container.ProjectId.ToString());
            if (!containerRemovalResult.IsFinishedSuccessfully)
            {
                continue;
            }

            var imageRemovalResult = await _dockerClient.Image.DockerRmi.Remove(container.ProjectId.ToString());
            if (!imageRemovalResult.IsFinishedSuccessfully)
            {
                continue;
            }

            container.Status = ContainerStatus.Destroyed;
            container.DeletedAt = DateTime.UtcNow;
        }

        await _dbContext.SaveChangesAsync();
    }
}
﻿using ContainerOrchestrator.BackgroundServices.Jobs.Handlers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace ContainerOrchestrator.BackgroundServices.Jobs;

public class ContainerManagerJob : BackgroundService
{
    private readonly IServiceScopeFactory _scopeFactory;
    private readonly ILogger _logger;
    private const int DelaySeconds = 60;

    public ContainerManagerJob(IServiceScopeFactory scopeFactory, ILogger logger)
    {
        _scopeFactory = scopeFactory;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var handler = scope.ServiceProvider.GetRequiredService<ContainerManagerHandler>();
                await handler.Handle();
            }
            catch (Exception exception)
            {
                _logger.Error("An exception occured in ContainerManagerJob", exception);
            }

            await Task.Delay(TimeSpan.FromSeconds(DelaySeconds), stoppingToken);
        }
    }
}
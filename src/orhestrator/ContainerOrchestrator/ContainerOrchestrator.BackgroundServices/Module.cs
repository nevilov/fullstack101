﻿using ContainerOrchestrator.BackgroundServices.Jobs;
using ContainerOrchestrator.BackgroundServices.Jobs.Handlers;
using Microsoft.Extensions.DependencyInjection;

namespace ContainerOrchestrator.BackgroundServices;

public static class Module
{
    public static IServiceCollection AddBackgroundServicesModule(this IServiceCollection services)
    {
        services.AddHostedService<ContainerManagerJob>();
        services.AddScoped<ContainerManagerHandler>();
        return services;
    }
}
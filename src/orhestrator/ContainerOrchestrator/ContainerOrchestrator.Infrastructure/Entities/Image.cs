﻿namespace ContainerOrchestrator.Infrastructure.Entities;

public class Image
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string? Source { get; set; }
    public string? Description { get; set; }
    public string TemplateDockerfile { get; set; }
}
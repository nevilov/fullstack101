﻿namespace ContainerOrchestrator.Infrastructure.Entities;

public class Container
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string? Port { get; set; }
    public int LifetimeMinute { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime? LaunchedAt { get; set; }
    public Image Image { get; set; }
    public string? BuildOutput { get; set; }
    
    public DateTime? DeletedAt { get; set; }
    public ContainerStatus Status { get; set; }
    public bool InSetupProgress { get; set; }
    public Guid ProjectId { get; set; }
}

public enum ContainerStatus
{
    InQueue,
    ImageCreationInQueue,
    ImageCreationFailed,
    ContainerCreationInQueue,
    ContainerCreationFailed,
    ProgramFailed,
    Running,
    Terminated,
    Killed,
    Destroyed
}
﻿using System.Collections;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ContainerOrchestrator.Infrastructure;

public static class Module
{
    public static IServiceCollection AddInfrastructureModule(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<OrchestratorDbContext>(opt =>
            opt.UseNpgsql(configuration.GetConnectionString("Default"), x=> x.MigrationsHistoryTable("__EFMigrationsHistory", "orchestrator")));
        
        return services;
    }
}
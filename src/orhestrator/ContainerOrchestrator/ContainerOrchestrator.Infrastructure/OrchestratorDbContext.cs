﻿using ContainerOrchestrator.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;

namespace ContainerOrchestrator.Infrastructure;

public class OrchestratorDbContext : DbContext
{
    public OrchestratorDbContext(DbContextOptions<OrchestratorDbContext> options) : base(options)
    {
    }

    public DbSet<Container> Containers { get; set; }
    public DbSet<Image> Images { get; set; }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.HasDefaultSchema("orchestrator");
    }
}
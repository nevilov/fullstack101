﻿(function(window) {
  window["env"] = window["env"] || {};

  window["env"]["apiUrl"] = "https://localhost:7146/api";
  window["env"]["orchestratorApiUrl"] =  "https://localhost:7024/api";
})(this);

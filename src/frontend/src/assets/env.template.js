(function(window) {
  window["env"] = window["env"] || {};

  window["env"]["apiUrl"] = "${apiUrl}";
  window["env"]["orchestratorApiUrl"] = "${orchestratorApiUrl}";
})(this);

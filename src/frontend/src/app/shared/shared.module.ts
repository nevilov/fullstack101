import {NgModule} from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {TopBarComponent} from "./top-bar/top-bar.component";
import {SideBarComponent} from './side-bar/side-bar.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {MatInputModule} from "@angular/material/input";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatButtonModule} from "@angular/material/button";
import {RouterLink} from "@angular/router";
import {EditorComponent} from './editor/editor.component';
import {EditorParserComponent} from './editor-parser/editor-parser.component';
import {ImagesUploaderComponent} from "./image-uploader/images-uploader.component";
import { CodeEditorComponent } from './code-editor/code-editor.component';
import {MonacoEditorModule} from "ngx-monaco-editor-v2";
import {FormsModule} from "@angular/forms";
import { FileExplorerComponent } from './code-editor/file-explorer/file-explorer.component';
import { NgxMonacoTreeComponent } from './code-editor/file-explorer/ngx-mocano-tree/ngx-monaco-tree.component';
import { MonacoTreeFileComponent } from './code-editor/file-explorer/ngx-mocano-tree/monaco-tree-file/monaco-tree-file.component';
import { MonacoTreeContextMenuComponent } from './code-editor/file-explorer/ngx-mocano-tree/monaco-tree-context-menu/monaco-tree-context-menu.component';
import {MatSelectModule} from "@angular/material/select";
import { TerminalComponent } from './code-editor/terminal/terminal.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";

@NgModule({
  declarations: [
    TopBarComponent,
    SideBarComponent,
    EditorComponent,
    EditorParserComponent,
    ImagesUploaderComponent,
    CodeEditorComponent,
    FileExplorerComponent,
    NgxMonacoTreeComponent,
    MonacoTreeFileComponent,
    MonacoTreeContextMenuComponent,
    TerminalComponent
  ],
  exports: [
    TopBarComponent,
    SideBarComponent,
    EditorComponent,
    EditorParserComponent,
    ImagesUploaderComponent,
    CodeEditorComponent
  ],
    imports: [
        CommonModule,
        MatFormFieldModule,
        FontAwesomeModule,
        NgOptimizedImage,
        MatInputModule,
        MatIconModule,
        MatSlideToggleModule,
        MatButtonModule,
        RouterLink,
        MonacoEditorModule.forRoot(),
        FormsModule,
        MatSelectModule,
        MatProgressSpinnerModule,
    ]
})

export class SharedModule {
}

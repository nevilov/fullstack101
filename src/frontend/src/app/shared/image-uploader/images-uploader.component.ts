import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ImagesUploaderPrompt} from "./images-uploader.prompt";

@Component({
  selector: 'app-uploader',
  templateUrl: './images-uploader.component.html',
  styleUrls: ['./images-uploader.component.scss']
})
export class ImagesUploaderComponent implements OnInit {

  @Input()
  prompt: ImagesUploaderPrompt = {
    title: 'Загрузить картинку'
  }

  @Output()
  imageFiles: EventEmitter<File[]> = new EventEmitter<File[]>();
  images: Array<{imagePreview: string, file: File}> = new Array<any>();

  constructor() { }
  ngOnInit(): void {
  }

  onSelectImage($event: any) {
    let files = $event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          if(e.target.result){
            this.images.push({
              imagePreview: e.target.result,
              file: $event.target.files[0]
            });
            this.emitImageList();
          }
        }
        reader.readAsDataURL(file);
      }
    }
  }

  emitImageList(){
    const imageFiles = this.images.map(x => x.file);
    this.imageFiles.emit(imageFiles)
  }

  onImageRemove(image: any) {
    this.images = this.images.filter(x => x != image);
  }
}

import {Component, Input, OnInit} from '@angular/core';
import {getEditorTheme} from "../../core/helpers/app.helper";
import {FileClick} from "./file-explorer/ngx-mocano-tree/monaco-tree-file/monaco-tree-file.component";
import {ToastrService} from "ngx-toastr";
import {Platform, ProjectDto, SaveProjectDto} from "../../core/dtos/project.dto";
import {ProjectService} from "../../core/services/http/project.service";
import {AuthService} from "../../core/services/auth.service";
import {ActivatedRoute, Router} from "@angular/router";
import {interval, switchMap} from "rxjs";
import {ContainerService} from "../../core/services/http/container.service";
import {ContainerStatus} from "../../core/dtos/container.dto";
import {formatExceptionMessage} from "../../core/helpers/exception.helper";

export interface ProjectElement {
  name: string;
  children?: ProjectElement[];
  content?: string;
  language?: string
}

@Component({
  selector: 'app-code-editor',
  templateUrl: './code-editor.component.html',
  styleUrls: ['./code-editor.component.scss'],
})
export class CodeEditorComponent implements OnInit {
  project: ProjectDto = {} as any;
  fileTree: ProjectElement[] = [];
  availablePlatforms: Platform[] = [];

  editorOptions = {theme: getEditorTheme(), language: 'csharp', readOnly: false};
  code: string = '';
  currentFilePath: string = '';
  containerLogs: string[] = [];
  isLoading: boolean = false;

  @Input()
  inputId: string = '';

  constructor(private toastr: ToastrService,
              private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _containerService: ContainerService,
              private _authService: AuthService,
              private _projectService: ProjectService) {
  }

  ngOnInit(): void {
    if(this.inputId){
      this.fetchData(this.inputId);
      return;
    }

    this._activatedRoute.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.fetchData(id);
      }
    })

  }

  fetchData(id: string) {
    console.log('fetching data')
    this._projectService
      .get(id)
      .subscribe(proj => {
        this.project = proj;
        this.fileTree = (JSON.parse(proj.jsonTree) as ProjectElement[])
          .sort((a, b) => {
            console.log('sorting')
            const fileNameA = a.name.toLowerCase();
            const fileNameB = a.name.toLowerCase();

            if (a.children && !b.children) {
              return -1;
            }

            if (!a.children && b.children) {
              return 1;
            }

            if (fileNameA < fileNameB) {
              return 1;
            }

            if (fileNameA > fileNameB) {
              return -1;
            }

            return 0;
          });
        this.editorOptions = {
          ...this.editorOptions,
          readOnly: !this.isCreatedByCurrentUser()
        }
      })

    this._projectService.getPlatforms()
      .subscribe(platforms => this.availablePlatforms = platforms);
  }

  handleContainerLongPoling() {
    const longPolingObservable = interval(5000)
      .pipe(
        switchMap(() => this._containerService.get(this.project.id, 1)),
      )
      .subscribe({
        next: (data) => {
          this.containerLogs = [
            data.buildOutput + '\n',
            data.lastContainerLogs,
          ]

          if(!data.isInProgress){
            longPolingObservable.unsubscribe();

            if ([ContainerStatus.Running, ContainerStatus.ImageCreationFailed, ContainerStatus.ContainerCreationFailed]
              .includes(data.status)) {
              this.toastr.info(`Завершено статус ${data.status.toString()}`)
            }
          }
        },
      });
  }

  onChangeCode() {
    this.saveFileState();
  }

  onFileSwitch(fileToBeSwitched: FileClick) {
    this.saveFileState();

    const filePath = fileToBeSwitched.filePath.split('/').filter(x => !!x);
    const fileNode = this.getNode(this.fileTree, filePath);
    if (!fileNode) {
      return;
    }

    this.code = fileNode.content!;
    this.currentFilePath = fileToBeSwitched.filePath;
    this.editorOptions = {...this.editorOptions, language: fileNode.language ?? 'csharp'}
  }

  saveFileState() {
    const filePath = this.currentFilePath.split('/').filter(x => !!x);
    const node = this.getNode(this.fileTree, filePath)
    if (!node) {
      return;
    }

    node!.content = this.code;
  }

  getNode(tree: ProjectElement[], pathSegments: string[], deep: number = 0): ProjectElement | null {
    for (const node of tree) {
      if (node.name === pathSegments[deep]) {
        if (!node.children) {
          return node;
        }
        return this.getNode(node.children, pathSegments, deep += 1)
      }
    }

    return null;
  }

  onSaveClick() {
    this.isLoading = true;
    this.project.jsonTree = JSON.stringify(this.fileTree);
    const saveProjectDto: SaveProjectDto = {
      ...this.project,
      platformName: this.project.platform.name
    }

    this._projectService.save(saveProjectDto).subscribe({
        next: () => {
          this.toastr.success('Проект успешно сохранен!', 'Редактор кода')
        },
        error: (err) => {
          this.toastr.error(formatExceptionMessage(err))
        },
        complete: () => this.isLoading = false
      }
    );
  }

  onRunClick() {
    this.isLoading = true;

    this.project.jsonTree = JSON.stringify(this.fileTree);
    const saveProjectDto: SaveProjectDto = {
      ...this.project,
      platformName: this.project.platform.name
    }

    this.toastr.info('Проект поставлен в очередь на запуск', 'Редактор кода');

    this._projectService.run(saveProjectDto).subscribe({
      next: () => {
        this.handleContainerLongPoling();
      },
      error: (err) => {
        this.toastr.error(formatExceptionMessage(err));
        this.isLoading = false;
      },
      complete: () => this.isLoading = false
    });
  }

  onArgumentChange($event: Event) {
    this.project.arguments = ($event.target as HTMLInputElement).value
  }

  onCommandChange($event: Event) {
    this.project.command = ($event.target as HTMLInputElement).value
  }

  isCreatedByCurrentUser() {
    return !!this.project.createdByUserName && this.project.createdByUserName.toLowerCase() == this._authService.getUser().userName.toLowerCase()
  }

  onCloneClick() {
    this.isLoading = true;

    this._projectService.clone(this.project.id).subscribe(newId => {
      this._router.navigateByUrl(`projects/sandbox/${newId}`);
      this.toastr.info('Проект успешно склонирован!', 'Редактор кода')
      this.isLoading = false;
    })
  }
}

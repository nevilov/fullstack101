import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ContextMenuAction, FileClick} from "./monaco-tree-file/monaco-tree-file.component";

@Component({
  selector: 'monaco-tree',
  templateUrl: './ngx-monaco-tree.component.html',
  styleUrls: ['./ngx-monaco-tree.component.scss']
})

export class NgxMonacoTreeComponent {
  @Input() theme: 'vs-dark' | 'vs-light' = 'vs-dark';
  @Input() tree: MonacoTreeElement[] = [];

  @Input() width = "300px"
  @Input() height = "100%"

  @Output() clickFile = new EventEmitter<FileClick>();
  @Output() clickContextMenu = new EventEmitter<ContextMenuAction>();
  handleClickFile(fileClick: FileClick) {
    this.clickFile.emit(fileClick);
  }

  handleClickContextMenu(event: ContextMenuAction) {
    this.clickContextMenu.emit(event);
  }
}

export type MonacoTreeElement = {
  name: string;
  language?: string;
  children?: MonacoTreeElement[]
}

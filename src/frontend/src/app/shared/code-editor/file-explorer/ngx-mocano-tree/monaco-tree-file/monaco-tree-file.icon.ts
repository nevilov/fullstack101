﻿export const fileIcons = {
  'cs': 'csharp',
  'json': 'json',
  'css': 'css',
  'angular': 'angular',
  'html': 'html',
  'js': 'javascript',
  'ts': 'typescript',
  'yml': 'yaml'
}

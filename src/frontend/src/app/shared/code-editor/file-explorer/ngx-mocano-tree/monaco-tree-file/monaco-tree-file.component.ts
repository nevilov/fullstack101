import {Component, ElementRef, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {MonacoTreeElement} from "../ngx-monaco-tree.component";
import {
  ContextMenuElementSeparator,
  ContextMenuElementText
} from "../monaco-tree-context-menu/monaco-tree-context-menu.component";
import {fileIcons} from "./monaco-tree-file.icon";

function getAbsolutePosition(element: any) {
  const r = { x: element.offsetLeft, y: element.offsetTop };
  // if (element.offsetParent) {
  //   const tmp = getAbsolutePosition(element.offsetParent);
  //   r.x += tmp.x;
  //   r.y += tmp.y;
  // }
  return r;
};

@Component({
  selector: 'monaco-tree-file',
  templateUrl: './monaco-tree-file.component.html',
  styleUrls: ['./monaco-tree-file.component.scss']
})
export class MonacoTreeFileComponent {
  @Input() name = '';
  @Input() children: MonacoTreeElement[]|undefined|null = undefined;
  @Input() depth = 0;
  @Input() theme: 'vs-dark'|'vs-light' = 'vs-dark';
  @Input() hide = false;

  @Output() clickFile = new EventEmitter<FileClick>();
  @Output() contextMenuClick = new EventEmitter<ContextMenuAction>();

  open = false;
  position: [number, number]|undefined = undefined;


  constructor(private eRef: ElementRef) {
  }

  contextMenu: Array<ContextMenuElementSeparator|ContextMenuElementText> = [
    {type: "element", name: 'New File', action: () => {
        this.contextMenuClick.emit(["new_file", this.name])
        this.position = [-1000, -1000];
      } },
    {type: "element", name: 'New Directory', action: () => {
        this.contextMenuClick.emit(["new_directory", this.name])
        this.position = [-1000, -1000];
      } },
    {type: "separator" },
    {type: "element", name: 'Rename', action: () => {
        this.contextMenuClick.emit(["rename_file", this.name])
        this.position = [-1000, -1000];
      } },
    {type: "element", name: 'Delete', action: () => {
        this.contextMenuClick.emit(["delete_file", this.name])
        this.position = [-1000, -1000];
      } }
  ]

  toggle() {
    this.open = !this.open;
    this.handleClickFile({filePath: '', isFolder: false});
  }

  get style() {
    return 'margin-left: '+ 10*this.depth +'px'
  }

  get folder() {
    return this.children !== null && this.children !== undefined
  }

  handleClickFile(file: FileClick) {
    const filePath = this.name + '/' + file.filePath;
    this.clickFile.emit({filePath, isFolder: this.folder});
  }

  handleRightClickFile(event: MouseEvent) {
    event.preventDefault()
    const pos = getAbsolutePosition(event.target);
    this.position = [pos.x + event.offsetX, pos.y + event.offsetY]
  }

  handleRightClick(event: ContextMenuAction) {
    this.contextMenuClick.emit([event[0], this.name + '/' + event[1]]);
  }

  @HostListener('document:contextmenu', ['$event'])
  clickOut(event: MouseEvent) {
    if(!this.eRef.nativeElement.contains(event.target)) {
      this.position = [-1000, -1000];
    }
  }

  get icon() {
    if(this.folder){
      return this.open ? 'folder-open' : 'folder';
    }

    const fileExtension = this.name.split('.').pop()!;
    if(Object.keys(fileIcons).includes(fileExtension)){
      return fileIcons[fileExtension as keyof typeof fileIcons];
    }

    return 'file'
  }
}

export type ContextMenuAction = ['new_file'|'new_directory'|'delete_file'|'rename_file', string];
export type FileClick ={
  filePath: string;
  isFolder: boolean;
}

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MonacoTreeElement} from "./ngx-mocano-tree/ngx-monaco-tree.component";
import {ContextMenuAction, FileClick} from "./ngx-mocano-tree/monaco-tree-file/monaco-tree-file.component";
import {getEditorTheme, isDarkTheme} from "../../../core/helpers/app.helper";

@Component({
  selector: 'app-file-explorer',
  templateUrl: './file-explorer.component.html',
  styleUrls: ['./file-explorer.component.scss']
})
export class FileExplorerComponent {
  currentFile = '';

  @Input()
  public tree: MonacoTreeElement[] = [];

  @Output()
  selectedFile: EventEmitter<FileClick> = new EventEmitter<FileClick>();

  handleContextMenu(action: ContextMenuAction) {
    console.log('handleContextMenu')
    if (action[0] === 'new_directory') {
      this.create('directory', action[1], this.tree);
    } else if (action[0] === 'new_file') {
      this.create('file', action[1], this.tree);
    } else if (action[0] === 'delete_file') {
      this.remove(action[1], this.tree);
    } else if (action[0] === 'rename_file') {
      this.rename(action[1], this.tree);
    }
  }

  rename(path: string, localTree: MonacoTreeElement[]) {
    const spited = path.split('/');
    if (spited.length === 1) {
      const file = localTree.find((el) => el.name == path);
      const filename = window.prompt('rename');
      if (filename && file) file.name = filename;
    } else {
      const file = localTree.find((el) => el.name == spited[0]);
      if (!file || !file.children) return;
      this.rename(spited.slice(1).join('/'), file?.children);
    }
  }

  remove(path: string, localTree: MonacoTreeElement[]) {
    const spited = path.split('/');
    if (spited.length === 1) {
      const index = localTree.findIndex((el) => el.name == path);
      localTree.splice(index, 1);
    } else {
      const file = localTree.find((el) => el.name == spited[0]);
      if (!file || !file.children) return;
      this.remove(spited.slice(1).join('/'), file?.children);
    }
  }

  create(
    type: 'directory' | 'file',
    path: string,
    localTree: MonacoTreeElement[]
  ) {
    const spited = path.split('/');
    const filename = window.prompt('Введите имя файла', 'file.html');
    if (!filename) return;
    if (spited.length === 1) {
      const file = localTree.find((el) => el.name == path);
      if (!file) return;
      else if (file.children === undefined) {
        localTree.push({
          name: filename,
          children: type === 'directory' ? [] : undefined,
        });
      } else {
        file.children.push({
          name: filename,
          children: type === 'directory' ? [] : undefined,
        });
      }
    } else {
      const file = localTree.find((el) => el.name == spited[0]);
      if (!file || !file.children) return;
      this.create(type, spited.slice(1).join('/'), file?.children);
    }
  }

  getTheme() {
    return getEditorTheme();
  }

  onFileSelected(fileClick: FileClick) {
    console.log('onFileSelected')

    this.selectedFile.emit(fileClick)
  }
}

import {Component, ElementRef, Input, ViewChild} from '@angular/core';
import {ProjectDto} from "../../../core/dtos/project.dto";

@Component({
  selector: 'app-terminal',
  templateUrl: './terminal.component.html',
  styleUrls: ['./terminal.component.scss']
})
export class TerminalComponent {
  @ViewChild('terminalPrompt')
  terminalPrompt!: ElementRef;

  @Input()
  isLoading = false;

  @Input()
  project: ProjectDto = {} as any;

  @Input()
  set setOutput(terminalOutput: string[]){
    if(!terminalOutput.length){
      return;
    }

    this.terminalOutputs.push({input: 'run action', output: this.formatOutput(terminalOutput)})
  }

  formatOutput(output: string[]): string{
    return `
      <pre>
        ${output.map(x => x)}
      <pre>
    `
  }

  public terminalOutputs: TerminalOutput[] = [];
  onAddPrompt() {
    if(!this.terminalPrompt){
      return;
    }

    const prompt = (this.terminalPrompt.nativeElement.value as string);
    const formatPrompt = prompt
      .trim()
      .toLowerCase();

    if(formatPrompt == 'clear'){
      this.onClear();
      return;
    }

    let result = '';
    switch (formatPrompt){
      case 'help':
        result = this.printHelp();
        break;
      case 'info':
        result = this.printInfo();
        break;
      default:
        result = 'Команда не найдена. Попробуй \'help\' чтобы увидеть доступные команды';
    }

    this.terminalOutputs.push({input: prompt, output: result});
    this.terminalPrompt.nativeElement.value = '';
    this.focusPromptInput();
  }

  printHelp(): string{
    return `<div>
        <div style="margin-bottom: 4px">Available commands: </div>
        <div>
            <div>help</div>
            <div>info -- Информация о проекте</div>
        </div>

    </div>
    `;
  }

//<div>project --name {name} (not available now)</div>

  printProjects(): string{
    return `<div>
        <div>Projects:</div>
        <div>
            <div></div><a class="link-v1" href="https://github.com/nevilov/DaraAdvertisements" target="_blank">1. DaraAdvertisements</a><div></div>
            <div><a class="link-v1" href="https://github.com/nevilov/DaraAdvertisements" target="_blank">2. Iteyka</a></div>
            <div><a class="link-v1" href="qaytarma.com">3. Qaytarma</a></div>
        </div>

    </div>`;
  }


  focusPromptInput() {
    this.terminalPrompt.nativeElement.focus();
    this.terminalPrompt.nativeElement.scrollIntoView();
  }

  onClear() {
    this.terminalOutputs = [];
    this.terminalPrompt.nativeElement.value = '';
  }

  private printInfo() {
    return `<div>
        <div>ProjectInfo:</div>
        <div>
            <div>${this.project.title}</div>
            <div>Описание: ${this.project.description}</div>
            <div>Создан пользователем ${this.project.createdByUserName}</div>
        </div>

    </div>`;
  }
}

interface TerminalOutput{
  input: string;
  output: string;
}

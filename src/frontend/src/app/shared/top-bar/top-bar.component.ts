import {Component, EventEmitter, Output} from '@angular/core';
import {MatSlideToggle, MatSlideToggleChange} from "@angular/material/slide-toggle";

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent {
  isChecked: boolean = localStorage.getItem('theme') == 'dark-theme';
  onDarkModeSwitch({checked}: MatSlideToggleChange){
    localStorage.setItem('theme', checked ? 'dark-theme' : 'light-theme');
  }

}

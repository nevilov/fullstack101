﻿import {OutputData} from "@editorjs/editorjs";

export default interface EditorPrompt {
  placeholder: string,
  data: OutputData
}

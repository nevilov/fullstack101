import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {editorInternalization, defaultMockData} from "../../core/configs/editorConfig";
import EditorJS, {OutputData} from "@editorjs/editorjs";
import Header from "@editorjs/header";
// @ts-ignore
import List from "@editorjs/list"
// @ts-ignore
import CodeTool from '@editorjs/code';
// @ts-ignore
import Warning from '@editorjs/warning';
// @ts-ignore
import Table from '@editorjs/table'
// @ts-ignore
import Paragraph from '@editorjs/paragraph';
// @ts-ignore
import Alignment from 'editorjs-text-alignment-blocktune'
// @ts-ignore
import Embed from '@editorjs/embed';
// @ts-ignore
import ImageTool from '@editorjs/image';
import {ImageService} from "../../core/services/http/image.service";

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})

export class EditorComponent implements AfterViewInit {
  @Input()
  editorPrompt: OutputData = defaultMockData;

  @Input({required:true})
  imageKey: string = 'images';

  @ViewChild('editor', { read: ElementRef, static: true})
  editorElement: ElementRef = {} as ElementRef;

  @Output()
  editorContent = new EventEmitter<OutputData>();

  constructor(public imageService: ImageService) {
  }

  private editorJs: EditorJS = {} as EditorJS;

  ngAfterViewInit(): void {
    this.editorJs = new EditorJS({
      data: this.editorPrompt,
      holder: this.editorElement.nativeElement,
      tools: {
        image: {
          class: ImageTool,
          config: {
            uploader: {
              uploadByFile: async (file: any) => {
                let data = await this.imageService.upload(file, this.imageKey ? `courses/${this.imageKey}` : 'courses/images');
                return {
                  success: 1,
                  file: {
                    url: data
                  }
                }
              },
              uploadByUrl: async (url: any) => {
                const image = await this.imageService.getFileByUrl(url + '?not-from-cache-please');
                console.log(image);
                let data = await this.imageService.upload(url, this.imageKey ? `courses/${this.imageKey}` : 'courses/images');
                return {
                  success: 1,
                  file: {
                    url: data
                  }
                }
              }
            }

          }
        },
        alignTune: {
          class: Alignment,
          config: {
            default: "left",
            blocks: {
              header: 'center',
              list: 'right'
            }
          },
        },
        paragraph: {
          class: Paragraph,
          inlineToolbar: true,
          config: {
            preserveBlank: true,
          },
          tunes: ['alignTune'],
        },
        header: {
          class: Header as any,
          inlineToolbar:true,
          tunes: ['alignTune']
        },
        embed: {
          class: Embed,
          config: {
            services: {
              youtube: true,
            }
          }
        },
        list: {
          class: List,
          inlineToolbar: true,
          config: {
            defaultStyle: 'unordered'
          },
          tunes: ['alignTune']
        },
        code: CodeTool,
        warning: {
          class: Warning,
          inlineToolbar: true,
          shortcut: 'CMD+SHIFT+W',
          config: {
            titlePlaceholder: 'Title',
            messagePlaceholder: 'Message',
          }
        },
        table: {
          class: Table,
        }
      },
      placeholder: 'Описание',
      i18n: editorInternalization
    });

  }

  onClick($event: MouseEvent) {
    this.editorJs.save().then((outputData) => {
      console.log(outputData)
      this.editorContent.emit(outputData)
    }).catch((error) => {
      console.error('Saving failed: ', error)
    });
  }
}

import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {faCodepen, faConnectdevelop, faFreeCodeCamp} from "@fortawesome/free-brands-svg-icons"
import {AuthService} from "../../core/services/auth.service";
import {User} from "../../core/dtos/user.dto";
import {MatSlideToggleChange} from "@angular/material/slide-toggle";
import {IconProp} from "@fortawesome/fontawesome-svg-core";

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  faConnectDevelopIcon = faConnectdevelop;
  faFreeCodeCampIcon = faFreeCodeCamp;
  user: User | null = null;

  constructor(public _authService: AuthService) {
  }

  ngOnInit(): void {
    this.user = this._authService.isAuthorized() ? this._authService.getUser() : null;
  }

  @Output()
  public onSideBarNarrow = new EventEmitter<boolean>();
  public isSideBarNarrow: boolean = JSON.parse(localStorage.getItem('isSideBarNarrow') as string) === true;

  onBarClick() {
    localStorage.setItem('isSideBarNarrow', !this.isSideBarNarrow ? "true" : "false");
    this.isSideBarNarrow = !this.isSideBarNarrow;
    this.onSideBarNarrow.emit(this.isSideBarNarrow);
  }

  logout() {
    this._authService.unAuthorize();
  }

  isAuthorized() {
    if(this._authService.isAuthorized()){
      this.user = this._authService.getUser();
      return true;
    }
    return false;
  }

  onProjectsTabOpen() {
    localStorage.setItem('isSideBarNarrow',"true");
    this.isSideBarNarrow = true;
    this.onSideBarNarrow.emit(this.isSideBarNarrow);
  }

  isDarkTheme: boolean = localStorage.getItem('theme') === 'dark-theme';
  onThemeSwitch(){
    localStorage.setItem('theme', localStorage.getItem('theme') == 'dark-theme' ? 'light-theme' : 'dark-theme');
    this.isDarkTheme = localStorage.getItem('theme') == 'dark-theme';
  }
}

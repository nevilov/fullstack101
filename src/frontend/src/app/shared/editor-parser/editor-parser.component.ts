import {Component, Input} from '@angular/core';
import {OutputData} from "@editorjs/editorjs";
import Parser from "editorjs-html";

@Component({
  selector: 'app-editor-parser',
  templateUrl: './editor-parser.component.html',
  styleUrls: ['./editor-parser.component.scss']
})
export class EditorParserComponent {
  @Input()
  set editorContent(edContent: OutputData) {
    const edjsParser = Parser({
      warning: this.warningParser,
      table: this.tableParser,
      image: this.imageParser,
      code: this.codeParser
    });

    const parsedValue = edjsParser.parse(edContent);
    this.content = parsedValue.reduce((a, c) => a + c, '');
  }

  content: string = '';

  warningParser(block: any) {
    return `<p> ${block.data.title} | ${block.data.message} </p>`;
  }

  codeParser(block: any) {
    return `
      <div class="code">${block.data.code}</div>
    `
  }

  imageParser(block: any){
    const styleClasses = block.data.withBackground ? 'image-with-background' : '';

    return `
    <div class="image ${styleClasses}">
      <img
          src="${block.data.file.url}"
          alt="${block.data.caption}"
        />
      </div>
    `
  }

  tableParser(block: any) {
    const arrayContent = block.data.content as string[][];
    const header = arrayContent[0];


    return `
       <div class="ed-table">
            ${arrayContent.map(row => {

      return `
              <div class="ed-row">
              ${row.map(element => `
                <div class="ed-cell">
                  ${element}
                </div>
              `).join('')}
            </div>
          `

    }).join('')}
        </div>
    `;
  }


}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ManagementComponent} from './management.component';
import {ManagementRoutingModule} from "./management-routing.module";
import {MatButtonModule} from "@angular/material/button";
import {MatDividerModule} from "@angular/material/divider";
import {MatIconModule} from "@angular/material/icon";
import {CourseManagementComponent} from './course-management/course-management.component';
import {AddCourseComponent} from './course-management/add-course/add-course.component';
import {MatInputModule} from "@angular/material/input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../../shared/shared.module";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {EditCourseComponent} from './course-management/edit-course/edit-course.component';
import {CdkDrag, CdkDropList} from "@angular/cdk/drag-drop";
import {FeaturesModule} from "../../features/features.module";
import {AddCourseItemComponent} from './course-management/edit-course/add-course-item/add-course-item.component';
import {MatRadioModule} from "@angular/material/radio";
import {EditCourseItemComponent} from './course-management/edit-course/edit-course-item/edit-course-item.component';
import {MatAutocompleteModule} from "@angular/material/autocomplete";

@NgModule({
  declarations: [
    ManagementComponent,
    CourseManagementComponent,
    AddCourseComponent,
    EditCourseComponent,
    AddCourseItemComponent,
    EditCourseItemComponent
  ],
  imports: [
    CommonModule,
    ManagementRoutingModule,
    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    SharedModule,
    FontAwesomeModule,
    CdkDropList,
    CdkDrag,
    FeaturesModule,
    MatRadioModule,
    FormsModule,
    MatAutocompleteModule,
  ],
  providers: []
})
export class ManagementModule {
}

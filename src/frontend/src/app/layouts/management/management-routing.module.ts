﻿import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ManagementComponent} from "./management.component";
import {CourseManagementComponent} from "./course-management/course-management.component";
import {AddCourseComponent} from "./course-management/add-course/add-course.component";
import {EditCourseComponent} from "./course-management/edit-course/edit-course.component";
import {AddCourseItemComponent} from "./course-management/edit-course/add-course-item/add-course-item.component";
import {EditCourseItemComponent} from "./course-management/edit-course/edit-course-item/edit-course-item.component";

const routes: Routes = [
  {
    path: '',
    component: ManagementComponent,
    children: [
      {
        path: '',
        redirectTo: 'course',
        pathMatch: 'full',
      },
      {
        path: 'course',
        component: CourseManagementComponent,
      },
      {
        path: 'course/add',
        component: AddCourseComponent
      },
      {
        path: 'course/edit/:slug',
        component: EditCourseComponent,
      },
      {
        path: 'course/edit/:slug/add-item',
        component: AddCourseItemComponent
      },
      {
        path: 'course/edit/:slug/edit-item/:id',
        component: EditCourseItemComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagementRoutingModule {
}

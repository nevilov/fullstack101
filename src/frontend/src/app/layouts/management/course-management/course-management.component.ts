import {Component, OnInit} from '@angular/core';
import {CourseService} from "../../../core/services/http/course.service";
import Course from "../../../core/dtos/course.dto";

@Component({
  selector: 'app-course-management',
  templateUrl: './course-management.component.html',
  styleUrls: ['./course-management.component.scss']
})
export class CourseManagementComponent implements OnInit{
  courses: Course[] = [];

  constructor(private _courseService: CourseService) {
  }

  ngOnInit(): void {
    this._courseService
      .get()
      .subscribe(data => this.courses = data);
  }

  onRemoveCourseClick(course: Course) {
    
  }
}

import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {CourseService} from "../../../../core/services/http/course.service";
import {ActivatedRoute} from "@angular/router";
import {OutputData} from "@editorjs/editorjs";
import {FormGroup, UntypedFormBuilder} from "@angular/forms";
import Course from "../../../../core/dtos/course.dto";
import {ToastrService} from "ngx-toastr";
import {CdkDragDrop, moveItemInArray} from "@angular/cdk/drag-drop";
import {ShortCourseItemDto} from "../../../../core/dtos/courseItem.dto";
import EditorPrompt from "../../../../shared/editor/editor.prompt";

@Component({
  selector: 'app-edit-course',
  templateUrl: './edit-course.component.html',
  styleUrls: ['./edit-course.component.scss'],
})
export class EditCourseComponent implements OnInit {
  editorContent: OutputData = null as any;
  selectedImages: File[] = [];
  existingCourse: Course = {} as any;
  formGroup: FormGroup;
  courseItems: ShortCourseItemDto[] = [];

  constructor(
    private courseService: CourseService,
    private router: ActivatedRoute,
    private fb: UntypedFormBuilder,
    private toastr: ToastrService) {

    this.formGroup = this.fb.group({
      id: '',
      name: '',
      slug: '',
      description: '',
      price: '',
      slightDescription: ''
    });
  }

  onEditorContentSaved($event: OutputData) {
    this.formGroup.value.description = JSON.stringify($event);
    this.editorContent = $event
  }
  ngOnInit(): void {
    const slug = this.router.snapshot.paramMap.get('slug');
    if (!slug) {
      return;
    }

    this.courseService
      .getBySlug(slug)
      .subscribe(data => {
        this.existingCourse = data;
        this.editorContent = JSON.parse(data.descriptionJson);
        this.courseItems = data.items.sort((x, y) => x.order - y.order);
        this.formGroup = this.fb.group({
          id: data.id,
          name: data.name,
          slug: data.slug,
          description: data.descriptionJson,
          price: data.price,
          slightDescription: data.slightDescription
        });
      });
  }

  onSendClick() {
    let formData = new FormData();
    formData.set('id', this.formGroup.value.id)
    formData.set('name', this.formGroup.value.name);
    formData.set('slug', this.formGroup.value.slug);
    formData.set('descriptionJson', this.formGroup.value.description);
    formData.set('price', this.formGroup.value.price);
    formData.set('slightDescription', this.formGroup.value.slightDescription);

    if(this.selectedImages[0]){
      formData.set('coverImageFile', this.selectedImages[0]);
    }

    this.existingCourse.items
      .forEach((x, i) => x.order = i);
    this.courseService
      .updateItemsOrder(this.formGroup.value.id, this.existingCourse.items)
      .subscribe();

    this.courseService
      .update(formData)
      .subscribe(next => {
        this.toastr.success('Курс успешно обновлен', 'Курсы')
      })
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.existingCourse.items, event.previousIndex, event.currentIndex);
  }


}

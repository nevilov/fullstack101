import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {OutputData} from "@editorjs/editorjs";
import {FormGroup, UntypedFormBuilder} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {ActivatedRoute} from "@angular/router";
import {SaveCourseItemDto, ItemType} from "../../../../../core/dtos/courseItem.dto";
import {ProjectService} from "../../../../../core/services/http/project.service";
import {ProjectDto} from "../../../../../core/dtos/project.dto";
import {CourseItemService} from "../../../../../core/services/http/courseItem.service";
import {Location} from '@angular/common';
import {formatExceptionMessage} from "../../../../../core/helpers/exception.helper";
import {CourseService} from "../../../../../core/services/http/course.service";

@Component({
  selector: 'app-add-course-item',
  templateUrl: './add-course-item.component.html',
  styleUrls: ['./add-course-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AddCourseItemComponent implements OnInit{
  protected readonly formGroup: FormGroup;
  editorContent: OutputData = null as any;
  projects: ProjectDto[] = [];

  constructor(private fb: UntypedFormBuilder,
              private projectService: ProjectService,
              private router: ActivatedRoute,
              private courseService: CourseService,
              private location: Location,
              private courseItemService: CourseItemService,
              private toastr: ToastrService) {
    this.formGroup = this.fb.group({
      title: '',
      courseId: '',
      order: 0,
      itemType: ItemType.Code,
      contentJson: '',
      projectId: null
    });
  }

  ngOnInit(): void {
    const slug = this.router.snapshot.paramMap.get('slug');
    if (!slug) {
      return;
    }

    this.projectService
      .getAll({filterByUser: false})
      .subscribe(data => this.projects = data);

    this.courseService.getBySlug(slug)
      .subscribe(data =>
        this.formGroup.get('courseId')?.setValue(data.id));
  }

  onEditorContentSaved($event: OutputData) {
    this.formGroup.value.contentJson = JSON.stringify($event);
    this.editorContent = $event
  }

  protected readonly ItemType = ItemType;

  onCreateClick() {
    const dto: SaveCourseItemDto = {
      title: this.formGroup.value.title,
      itemType: Number(this.formGroup.value.itemType),
      contentJson: this.formGroup.value.contentJson,
      courseId: this.formGroup.value.courseId,
      order: this.formGroup.value.order,
      projectId: this.formGroup.value.projectId
    }

    this.courseItemService.create(dto)
      .subscribe({
        next: () => {
          this.toastr.success('Элемент успешно добавлен!', 'Курс менеджмент');
          this.location.back();
        },
        error: (err) => {
          this.toastr.error(formatExceptionMessage(err))
        },
      })
  }

  isSectionSelected(itemType: ItemType) {
    return this.formGroup.get('itemType')?.value == itemType;
  }
}

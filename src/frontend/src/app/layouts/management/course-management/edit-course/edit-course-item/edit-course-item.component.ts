import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, UntypedFormBuilder} from "@angular/forms";
import {OutputData} from "@editorjs/editorjs";
import {ProjectDto} from "../../../../../core/dtos/project.dto";
import {ProjectService} from "../../../../../core/services/http/project.service";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";
import {CourseItemService} from "../../../../../core/services/http/courseItem.service";
import {ToastrService} from "ngx-toastr";
import {ItemType, SaveCourseItemDto} from "../../../../../core/dtos/courseItem.dto";
import {formatExceptionMessage} from "../../../../../core/helpers/exception.helper";
import {map, Observable, startWith} from "rxjs";

@Component({
  selector: 'app-edit-course-item',
  templateUrl: './edit-course-item.component.html',
  styleUrls: ['./edit-course-item.component.scss'],
})
export class EditCourseItemComponent implements OnInit{
  protected formGroup: FormGroup;
  editorContent: OutputData = null as any;
  projects: ProjectDto[] = [];
  projectControl: FormControl = new FormControl();
  filteredProjects: Observable<ProjectDto[]> = new Observable<ProjectDto[]>();
  courseItemId: string = '';

  constructor(private fb: UntypedFormBuilder,
              private projectService: ProjectService,
              private router: ActivatedRoute,
              private location: Location,
              private courseItemService: CourseItemService,
              private toastr: ToastrService) {
    this.formGroup = this.fb.group({
      title: '',
      courseId: '',
      order: 0,
      itemType: ItemType.Code,
      contentJson: '',
      projectId: null
    });

    this.projectControl = new FormControl();
  }

  ngOnInit(): void {
    const id = this.router.snapshot.paramMap.get('id');
    if (!id) {
      return;
    }

    this.courseItemId = id;

    this.projectService
      .getAll({filterByUser: false})
      .subscribe(data => this.projects = data);

    this.courseItemService.get(id)
      .subscribe(data => {
        if(data.contentJson){
          this.editorContent = JSON.parse(data.contentJson);
        }

        this.formGroup = this.fb.group({
          title: data.title,
          courseId: data.courseId,
          order: data.order,
          itemType: data.itemType,
          contentJson: data.contentJson,
          projectId: data.project
        });

        this.projectControl.setValue(data.project?.id);
      })


    this.filteredProjects = this.projectControl.valueChanges.pipe(
      startWith(''),
      map((project) =>
        project ? this.filterProject(project) : this.projects.slice()
      )
    );
  }


  filterProject(name: string) {
    return this.projects.filter(
      (project) => project.title.toLowerCase().indexOf(name.toLowerCase()) === 0
    );
  }

  onEditorContentSaved($event: OutputData) {
    this.formGroup.value.contentJson = JSON.stringify($event);
    this.editorContent = $event
  }

  protected readonly ItemType = ItemType;

  onUpdateClick() {
    const dto: SaveCourseItemDto = {
      title: this.formGroup.value.title,
      itemType: Number(this.formGroup.value.itemType),
      contentJson: this.formGroup.value.contentJson,
      courseId: this.formGroup.value.courseId,
      order: this.formGroup.value.order,
      projectId: this.projectControl.value
    }

    this.courseItemService.update(this.courseItemId, dto)
      .subscribe({
        next: () => {
          this.toastr.success('Элемент успешно добавлен!', 'Курс менеджмент');
          this.location.back();
        },
        error: (err) => {
          this.toastr.error(formatExceptionMessage(err))
        },
      })
  }

  isSectionSelected(itemType: ItemType) {
    return this.formGroup.get('itemType')?.value == itemType;
  }
}

import { Component } from '@angular/core';
import {OutputData} from "@editorjs/editorjs";
import {FormGroup, UntypedFormBuilder} from "@angular/forms";
import {CourseService} from "../../../../core/services/http/course.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.scss']
})
export class AddCourseComponent {
  editorContent: OutputData = null as any;
  selectedImages: File[] = [];
  protected readonly formGroup: FormGroup;

  constructor(private fb: UntypedFormBuilder,
              private courseService: CourseService,
              private toastr: ToastrService) {
    this.formGroup = this.fb.group({
      name: '',
      slug: '',
      description: '',
      price: '',
      slightDescription: ''
    });
  }

  onEditorContentSaved($event: OutputData) {
    this.formGroup.value.description = JSON.stringify($event);
    this.editorContent = $event
  }

  onSendClick() {
    let formData = new FormData();
    formData.set('name', this.formGroup.value.name);
    formData.set('slug', this.formGroup.value.slug);
    formData.set('descriptionJson', this.formGroup.value.description);
    formData.set('price', this.formGroup.value.price);
    formData.set('slightDescription', this.formGroup.value.slightDescription);

    if(this.selectedImages[0]){
      formData.set('coverImageFile', this.selectedImages[0]);
    }

    this.courseService
      .create(formData)
      .subscribe(next => {
        this.toastr.success('Курс создался успешно', 'Курсы')
      })
  }
}

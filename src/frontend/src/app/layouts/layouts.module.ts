import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutsComponent } from './layouts.component';
import {SharedModule} from "../shared/shared.module";
import {RouterOutlet} from "@angular/router";
import {LayoutsRoutingModule} from "./layouts-routing.module";
import {ToastrModule} from "ngx-toastr";
import {FeaturesModule} from "../features/features.module";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {AuthService} from "../core/services/auth.service";

@NgModule({
  declarations: [
    LayoutsComponent,
  ],
  exports:[
    LayoutsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterOutlet,
    LayoutsRoutingModule,
    ToastrModule.forRoot({
      maxOpened: 5,
      positionClass: 'toast-bottom-right',
      closeButton: true,
      progressBar: true
    }),
    FeaturesModule,
    FontAwesomeModule,
  ],
  providers: [AuthService]
})
export class LayoutsModule { }

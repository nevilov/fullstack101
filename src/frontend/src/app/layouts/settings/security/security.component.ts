import { Component } from '@angular/core';
import {AbstractControl, FormControl, FormGroup} from "@angular/forms";
import {UserService} from "../../../core/services/http/user.service";
import {PasswordDto} from "../../../core/dtos/user.dto";
import {ToastrService} from "ngx-toastr";
import {formatExceptionMessage} from "../../../core/helpers/exception.helper";

@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.scss']
})
export class SecurityComponent {
  hidePassword: boolean = true;
  passwordForm: FormGroup = new FormGroup({
    oldPassword: new FormControl(''),
    newPassword: new FormControl(''),
    retryPassword: new FormControl('', {validators: [this.passwordConfirming]})
  });

  constructor(private _userService: UserService,
              private _toastr: ToastrService) {
  }
  passwordConfirming(c: AbstractControl): { invalid: boolean } {
    if (c.get('newPassword')?.value !== c.get('retryPassword')?.value) {
      console.log('valid')
      return {invalid: true};
    }
    console.log('not valid')

    return { invalid: false }
  }

  onUpdatePassword() {
    const dto: PasswordDto = {
      newPassword: this.passwordForm.value.newPassword,
      oldPassword: this.passwordForm.value.oldPassword,
      retryPassword: this.passwordForm.value.retryPassword
    }
    this._userService.changePassword(dto).subscribe({
      next: () => this._toastr.success('Пароль успешно изменен', 'FullStack101'),
      error: (err) => {
        this._toastr.error(formatExceptionMessage(err))
      },
    })
  }
}

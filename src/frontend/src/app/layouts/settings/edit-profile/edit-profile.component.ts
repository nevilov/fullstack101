import {Component, OnInit} from '@angular/core';
import { FormControl, FormGroup} from "@angular/forms";
import {UserService} from "../../../core/services/http/user.service";
import {toFormData} from "../../../core/helpers/form.helper";
import {ToastrService} from "ngx-toastr";
import {formatExceptionMessage} from "../../../core/helpers/exception.helper";
import {AuthService} from "../../../core/services/auth.service";
import {FormatWidth, getLocaleDateFormat} from "@angular/common";

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {
  editForm: FormGroup = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    userName: new FormControl({value: '', disabled: true}),
    email: new FormControl(''),
    birthDate: new FormControl(''),
    avatarUrl: new FormControl('')
  });
  private uploadedImage: File = null as any;
  constructor(
    private _userService: UserService,
    private _authService: AuthService,
    private _toastr: ToastrService) {
  }

  ngOnInit(): void {
    this._userService.get().subscribe(data => {
      this.editForm = new FormGroup({
        firstName: new FormControl( data.firstName),
        lastName: new FormControl(data.lastName),
        userName: new FormControl({value: data.userName, disabled: true}),
        email: new FormControl(data.email),
        birthDate: new FormControl(data.birthDate),
        avatarUrl: new FormControl(data.avatarUrl)
      });
    })
  }

  onUploadImageEvent($event: File) {
    this.uploadedImage = $event;
  }

  onSubmitChanges(){

    const tzoffset = (new Date()).getTimezoneOffset() * 60000;
    const localISOTime = (new Date(Date.parse(this.editForm.value.birthDate) - tzoffset)).toISOString().slice(0, -1);
    this.editForm.get('birthDate')?.setValue(localISOTime)

    const formData = toFormData(this.editForm);
    if(this.uploadedImage){
      formData.set('avatar', this.uploadedImage);
    }

    this._userService.update(formData).subscribe({
      next: () => {
        this._toastr.success('Данные успешно обновлены', 'FullStack101');
        this._userService.get().subscribe(user => this._authService.setUserData(user))
      },
      error: (err) => {
        this._toastr.error(formatExceptionMessage(err))
      },
    });
  }
}

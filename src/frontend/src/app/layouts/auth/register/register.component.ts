import { Component } from '@angular/core';
import {UserService} from "../../../core/services/http/user.service";
import {FormControl, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {AuthService} from "../../../core/services/auth.service";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  hidePassword: boolean = true;
  selectedAvatar: File = null as any;
  registerForm: FormGroup = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    userName: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl(''),
    birthDate: new FormControl(''),
  });

  constructor(private _userService: UserService,
              private _router: Router,
              private _toastr: ToastrService,
              private _authService: AuthService,
              private _datePipe: DatePipe) {
  }

  register(){
    let formData = new FormData();
    const convertedDate = this._datePipe.transform(this.registerForm.value.birthDate, 'yyyy-MM-dd', 'Europe/Moscow')!;
    formData.set('avatar', this.selectedAvatar);
    formData.set('firstName', this.registerForm.value.firstName);
    formData.set('lastName', this.registerForm.value.lastName);
    formData.set('userName', this.registerForm.value.userName);
    formData.set('email', this.registerForm.value.email);
    formData.set('password', this.registerForm.value.password);
    formData.set('birthDate', convertedDate);

    this._userService.register(formData).subscribe(data => {
      this._authService.setToken(data.token);
      this._authService.setUser(data.user, data.isAdmin);

      this._toastr.success('Регистраниция прошла успешно!', 'Авторизация');
      this._router.navigateByUrl('');
    })
  }

  onImageSelected($event: File[]) {
    this.selectedAvatar = $event[0];
  }
}

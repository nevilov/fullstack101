import { Component } from '@angular/core';
import {UserService} from "../../../core/services/http/user.service";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {AuthService} from "../../../core/services/auth.service";
import {DatePipe} from "@angular/common";
import {FormControl, FormGroup} from "@angular/forms";
import {LoginDto} from "../../../core/dtos/user.dto";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  hidePassword: boolean = true;

  loginForm: FormGroup = new FormGroup({
    login: new FormControl(''),
    password: new FormControl(''),
  });
  constructor(private _userService: UserService,
              private _router: Router,
              private _toastr: ToastrService,
              private _authService: AuthService) {
  }

  onLogin(){
    const loginDto: LoginDto = {
      login: this.loginForm.value.login,
      password: this.loginForm.value.password
    };

    this._userService.login(loginDto).subscribe(data => {
      this._authService.setToken(data.token);
      this._authService.setUser(data.user, data.isAdmin);
      this._toastr.success('Вы успешно авторизированы!', 'Авторизация');
      this._router.navigateByUrl('');
    });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {SandboxComponent} from "./sandbox/sandbox.component";
import { ProjectComponent } from './project.component';
import {MatButtonModule} from "@angular/material/button";

@NgModule({
  declarations: [
    SandboxComponent,
    ProjectComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ProjectComponent
      },
      {
        path: 'sandbox/:id',
        component: SandboxComponent
      }
    ]),
    SharedModule,
    MatButtonModule
  ]
})
export class ProjectModule { }

import {Component, OnInit} from '@angular/core';
import {ProjectService} from "../../core/services/http/project.service";
import {ProjectDto} from "../../core/dtos/project.dto";

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit{
  projects: ProjectDto[] = [];
  constructor(private _projectService: ProjectService) {
  }

  ngOnInit(): void {
    this._projectService.getAll({filterByUser: false}).subscribe(data => this.projects = data)
  }

}

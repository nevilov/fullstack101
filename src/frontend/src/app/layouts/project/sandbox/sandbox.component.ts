import {Component, OnInit} from '@angular/core';
import {ProjectDto} from "../../../core/dtos/project.dto";
import {ProjectService} from "../../../core/services/http/project.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-sandbox',
  templateUrl: './sandbox.component.html',
  styleUrls: ['./sandbox.component.scss']
})
export class SandboxComponent implements OnInit{

  id = '';
  constructor(private _projectService: ProjectService,
              private router: ActivatedRoute,) {
  }


  ngOnInit(): void {
    this.router.params.subscribe(params => {
      const id = params['id']
      if (!id) {
        this.id = '';
        return;
      }

      this.id = id;
    })
  }
}

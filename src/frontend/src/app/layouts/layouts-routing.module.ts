﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LayoutsComponent} from "./layouts.component";
import {DashboardComponent} from "./dashboard/dashboard.component";

const routes: Routes = [

  {
    path: '',
    component: LayoutsComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
      {
        path: 'auth',
        loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
      },
      {
        path: 'management',
        loadChildren: () => import('./management/management.module').then(m => m.ManagementModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'courses',
        loadChildren: () => import('./course/course.module').then(x => x.CourseModule)
      },
      {
        path: 'settings',
        loadChildren: () => import('./settings/settings.module').then(x => x.SettingsModule)
      },
      {
        path: 'projects',
        loadChildren: () => import('./project/project.module').then(x => x.ProjectModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutsRoutingModule { }

import {Component, Input, OnInit} from '@angular/core';
import Course from "../../../core/dtos/course.dto";
import {CourseService} from "../../../core/services/http/course.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.scss']
})
export class CourseDetailComponent implements OnInit{
  @Input()
  course: Course = null as any;

  constructor(
    private courseService: CourseService,
    private router: ActivatedRoute, ) {
  }

  getDescriptionJson(){
    return JSON.parse(this.course.descriptionJson);
  }

  ngOnInit(): void {
    const slug = this.router.snapshot.paramMap.get('slug');
    if(!slug){
      return;
    }

    this.courseService
      .getBySlug(slug)
      .subscribe(data => this.course = data);
  }
}

﻿import {RouterModule, Routes} from "@angular/router";
import {CourseComponent} from "./course.component";
import {NgModule} from "@angular/core";
import {CourseDetailComponent} from "./course-detail/course-detail.component";
import {CourseItemsComponent} from "./course-items/course-items.component";

const routes: Routes = [
  {
    path: '',
    component: CourseComponent
  },
  {
    path: ':slug',
    component: CourseDetailComponent
  },
  {
    path: ':slug/items/:id',
    component: CourseItemsComponent
  },
  {
    path: ':slug/items',
    component: CourseItemsComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class CourseRoutingModule { }

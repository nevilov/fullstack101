import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CourseComponent} from './course.component';
import {FeaturesModule} from "../../features/features.module";
import {CourseRoutingModule} from "./course-routing.module";
import { CourseDetailComponent } from './course-detail/course-detail.component';
import {SharedModule} from "../../shared/shared.module";
import {MatListModule} from "@angular/material/list";
import {MatButtonModule} from "@angular/material/button";
import { CourseItemsComponent } from './course-items/course-items.component';
import {MatSidenavModule} from "@angular/material/sidenav";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [
    CourseComponent,
    CourseDetailComponent,
    CourseItemsComponent
  ],
  imports: [
    CommonModule,
    CourseRoutingModule,
    FeaturesModule,
    SharedModule,
    MatListModule,
    MatButtonModule,
    MatSidenavModule,
    FontAwesomeModule
  ]
})
export class CourseModule {
}

import {Component, OnInit} from '@angular/core';
import {CourseService} from "../../../core/services/http/course.service";
import {CourseItemService} from "../../../core/services/http/courseItem.service";
import Course from "../../../core/dtos/course.dto";
import {CourseItemDto, ItemType, ShortCourseItemDto} from "../../../core/dtos/courseItem.dto";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-course-items',
  templateUrl: './course-items.component.html',
  styleUrls: ['./course-items.component.scss']
})
export class CourseItemsComponent implements OnInit{
  course: Course = null as any;
  courseItem: CourseItemDto = null as any;
  constructor(
    private courseService: CourseService,
    private router: ActivatedRoute,
    private courseItemService: CourseItemService
  ) {

  }

  ngOnInit(): void {
    const slug = this.router.snapshot.paramMap.get('slug');

    if(!slug){
      return;
    }

    this.courseService
      .getBySlug(slug)
      .subscribe(data => {
        this.course = {
          ...data,
          items: data.items.sort((a,b) => a.order -b.order)
        }
      });

    const itemId = this.router.snapshot.paramMap.get('id');
    if(!itemId){
      return;
    }

    this.courseItemService.get(itemId)
      .subscribe(data => this.courseItem = data);
    }

  getContentJson(){
    return JSON.parse(this.courseItem.contentJson!);
  }


  openCourseItem(item: ShortCourseItemDto) {
    this.courseItemService.get(item.id)
      .subscribe(data => this.courseItem = data);
  }

  protected readonly ItemType = ItemType;
}

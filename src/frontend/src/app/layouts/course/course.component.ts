import {Component, OnInit} from '@angular/core';
import Course from "../../core/dtos/course.dto";
import {CourseService} from "../../core/services/http/course.service";

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit{
  courses: Course[] = [];
  constructor(public courseService: CourseService) {
  }

  ngOnInit(): void {
    this.courseService.get().subscribe(data => {
      this.courses = data
    });
  }

  onCourseItemClick(course: Course) {

  }
}


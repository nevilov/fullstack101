import {Component, HostBinding} from '@angular/core';

@Component({
  selector: 'app-layouts',
  templateUrl: './layouts.component.html',
  styleUrls: ['./layouts.component.scss']
})
export class LayoutsComponent {
  public isSideBarNarrow: boolean = JSON.parse(localStorage.getItem('isSideBarNarrow') as string) === true;

  @HostBinding('class')
  get themeMode(){
    return localStorage.getItem('theme') ?? 'light-theme';
  }
  onSideBarNarrow($event: boolean) {
    this.isSideBarNarrow = $event;
  }
}

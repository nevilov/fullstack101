import {Component, OnInit} from '@angular/core';
import Course from "../../../core/dtos/course.dto";
import {CourseService} from "../../../core/services/http/course.service";
import {ProjectService} from "../../../core/services/http/project.service";
import {ProjectDto} from "../../../core/dtos/project.dto";

@Component({
  selector: 'app-my-projects',
  templateUrl: './my-projects.component.html',
  styleUrls: ['./my-projects.component.scss']
})
export class MyProjectsComponent implements OnInit {
  userProjects: ProjectDto[] = [];

  constructor(private _projectService: ProjectService) {
  }

  ngOnInit(): void {
    this._projectService.getAll({filterByUser: true})
      .subscribe(data => this.userProjects = data);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {MyProjectsComponent} from "./my-projects/my-projects.component";
import {MyCoursesComponent} from "./my-courses/my-courses.component";
import {DashboardComponent} from "./dashboard.component";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FeaturesModule} from "../../features/features.module";
import {MatButtonModule} from "@angular/material/button";



@NgModule({
  declarations: [
    DashboardComponent,
    MyCoursesComponent,
    MyProjectsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: DashboardComponent,
        children: [
          {
            path: '',
            redirectTo: 'courses',
            pathMatch: 'full'
          },
          {
            path: 'projects',
            component: MyProjectsComponent
          },
          {
            path: 'courses',
            component: MyCoursesComponent
          }
        ]
      }
    ]),
    FontAwesomeModule,
    FeaturesModule,
    MatButtonModule
  ]
})
export class DashboardModule { }

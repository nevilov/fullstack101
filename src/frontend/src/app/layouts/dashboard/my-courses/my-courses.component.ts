import {Component, OnInit} from '@angular/core';
import Course from "../../../core/dtos/course.dto";
import {CourseService} from "../../../core/services/http/course.service";

@Component({
  selector: 'app-my-courses',
  templateUrl: './my-courses.component.html',
  styleUrls: ['./my-courses.component.scss']
})
export class MyCoursesComponent implements OnInit{
  courses: Course[] = [];
  constructor(public courseService: CourseService) {
  }

  ngOnInit(): void {
    this.courseService.get().subscribe(data => {
      this.courses = data
    });
  }
}

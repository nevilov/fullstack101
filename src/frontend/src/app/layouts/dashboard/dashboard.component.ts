import {Component, OnInit} from '@angular/core';
import {CourseService} from "../../core/services/http/course.service";
import Course from "../../core/dtos/course.dto";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit{
  courses: Course[] = [];
  constructor(public courseService: CourseService) {
  }

  ngOnInit(): void {
    this.courseService.get().subscribe(data => {
      this.courses = data
    });
  }


}

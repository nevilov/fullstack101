﻿export interface CourseCardPrompt{
  title: string,
  slug: string,
  imgUrl: string
}

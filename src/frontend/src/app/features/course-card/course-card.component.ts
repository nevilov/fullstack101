import {Component, Input} from '@angular/core';
import Course from "../../core/dtos/course.dto";

@Component({
  selector: 'app-course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.scss']
})
export class CourseCardComponent {
  @Input({required: true})
  courseCardPrompt: Course = null as any;

}

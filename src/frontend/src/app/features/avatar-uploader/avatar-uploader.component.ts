import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-avatar-uploader',
  templateUrl: './avatar-uploader.component.html',
  styleUrls: ['./avatar-uploader.component.scss']
})
export class AvatarUploaderComponent {
  @Input()
  fileUrl: string = '';

  @Output()
  onUploadImageCallback: EventEmitter<File> = new EventEmitter<File>();

  onFileChange(event: any) {
    const files = event.target.files as FileList;
    if (files.length <= 0) {
      return;
    }

    this.fileUrl = URL.createObjectURL(files[0]);
    this.onUploadImageCallback.emit(files[0]);
    this.resetInput();

  }

  resetInput(){
    const input = document.getElementById('avatar-input-file') as HTMLInputElement;
    if(input){
      input.value = "";
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseCardComponent } from './course-card/course-card.component';
import {MatButtonModule} from "@angular/material/button";
import { AvatarUploaderComponent } from './avatar-uploader/avatar-uploader.component';
import {MatIconModule} from "@angular/material/icon";

@NgModule({
  declarations: [
    CourseCardComponent,
    AvatarUploaderComponent,
  ],
  exports: [
    CourseCardComponent,
    AvatarUploaderComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule
  ]
})
export class FeaturesModule { }

import {Component} from '@angular/core';
import {FaIconLibrary} from "@fortawesome/angular-fontawesome";
import {
  faCoffee,
  faHouse,
  faGraduationCap,
  faUser,
  faBars,
  faGear,
  faNewspaper,
  faRightFromBracket,
  faMoon,
  faSun,
  faChessKnight,
  faBell,
  faPen,
  faXmark, faAnglesLeft, faAnglesRight, faShieldHalved
} from "@fortawesome/free-solid-svg-icons";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(library: FaIconLibrary) {
    const icons = [
      faCoffee, faHouse, faGraduationCap, faUser, faBars, faGear, faNewspaper,
      faRightFromBracket, faMoon, faChessKnight, faBell,
      faPen, faXmark, faSun, faAnglesRight, faAnglesLeft,
      faShieldHalved
    ];
    library.addIcons(...icons);
  }
}

﻿import {Injectable} from "@angular/core";
import {environment} from "../../../../environments/env";
import {HttpClient} from "@angular/common/http";
import {CourseItemDto, SaveCourseItemDto} from "../../dtos/courseItem.dto";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class CourseItemService {
  private apiUrl = environment.apiUrl;
  private courseItemControllerUri = `${environment.apiUrl}/CourseItem`

  constructor(private _http: HttpClient) {
  }

  public create(courseItemDto: SaveCourseItemDto){
    return this._http.post(this.courseItemControllerUri, courseItemDto);
  }

  public update(id: string, courseItemDto: SaveCourseItemDto){
    return this._http.put(`${this.courseItemControllerUri}/${id}`, courseItemDto)
  }

  public get(id: string): Observable<CourseItemDto>{
    return this._http.get<CourseItemDto>(`${this.courseItemControllerUri}/${id}`);
  }
}

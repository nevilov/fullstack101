﻿import {Injectable} from "@angular/core";
import {environment} from "../../../../environments/env";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ContainerDto} from "../../dtos/container.dto";

@Injectable({
  providedIn: 'root'
})
export class ContainerService {
  private containerControllerUri = `${environment.orchestratorApiUrl}/container`

  constructor(private _http: HttpClient) {
  }

  public get(projectId: string, logsSinceMinutes: number): Observable<ContainerDto> {
    return this._http.get<ContainerDto>(`${this.containerControllerUri}/project/${projectId}?logsSinceMinutes=${logsSinceMinutes}`);
  }

}

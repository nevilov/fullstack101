﻿import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/env";
import {Observable} from "rxjs";
import {Platform, ProjectDto} from "../../dtos/project.dto";
import {RequestQueryDto} from "../../dtos/requestQueryDto";

@Injectable({
  providedIn: 'root'
})

export class ProjectService{
  private apiUrl = environment.apiUrl;
  private projectControllerUri = `${environment.apiUrl}/project`

  constructor(private _http: HttpClient) {
  }

  public get(id: string){
    return this._http.get<ProjectDto>(`${this.projectControllerUri}/${id}`);
  }

  public save(form: any){
    return this._http.post(this.projectControllerUri, form);
  }

  public run(form: any){
    return this._http.post(`${this.projectControllerUri}/run`, form);
  }

  public clone(id: string): Observable<string>{
    return this._http.post<string>(`${this.projectControllerUri}/clone/${id}`, {});
  }

  public getAll(queryRequest: RequestQueryDto): Observable<ProjectDto[]>{
    return this._http.get<ProjectDto[]>(`${this.projectControllerUri}?filterByUser=${queryRequest.filterByUser}`)
  }

  public getPlatforms(){
    return this._http.get<Platform[]>(`${this.projectControllerUri}/platforms`)
  }

}

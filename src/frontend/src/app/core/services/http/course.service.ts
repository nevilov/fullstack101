﻿import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/env";
import {Observable} from "rxjs";
import Course from "../../dtos/course.dto";
import {ShortCourseItemDto} from "../../dtos/courseItem.dto";

@Injectable({
  providedIn: 'root'
})

export class CourseService {
  private apiUrl = environment.apiUrl;
  private courseControllerUri = `${environment.apiUrl}/course`
  constructor(private _http: HttpClient) {
  }

  public create(form: FormData){
    return this._http.post(`${this.courseControllerUri}`, form);
  }

  public update(form: FormData){
    return this._http.put(`${this.courseControllerUri}`, form);
  }

  public getBySlug(slug: string){
    return this._http.get<Course>(`${this.courseControllerUri}/${slug}`);
  }

  public get(){
    return this._http.get<Course[]>(this.courseControllerUri);
  }

  public updateItemsOrder(id: string, items: ShortCourseItemDto[]){
    return this._http.post(`${this.courseControllerUri}/${id}/course-items/order`, items)
  }

}

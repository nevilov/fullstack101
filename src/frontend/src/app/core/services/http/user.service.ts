﻿import {Injectable} from "@angular/core";
import {environment} from "../../../../environments/env";
import {HttpClient} from "@angular/common/http";
import {AuthDto, LoginDto, PasswordDto, User} from "../../dtos/user.dto";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private authController = `${environment.apiUrl}/user`
  constructor(private _http: HttpClient) {
  }

  public login(loginDto: LoginDto): Observable<AuthDto>{
    return this._http.post<AuthDto>(`${this.authController}/login`, loginDto);
  }

  public register(registerForm: FormData): Observable<AuthDto>{
    return this._http.post<AuthDto>(`${this.authController}/register`, registerForm);
  }

  public update(form: FormData){
    return this._http.put(`${this.authController}/profile`, form);
  }

  public changePassword(passwordDto: PasswordDto){
    return this._http.put(`${this.authController}/password`, passwordDto);
  }
  public get(): Observable<User>{
    return this._http.get<User>(`${this.authController}/current`);
  }
}

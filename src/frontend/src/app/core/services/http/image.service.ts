﻿import {Injectable} from "@angular/core";
import {environment} from "../../../../environments/env";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class ImageService{
  private imageControllerUri = `${environment.apiUrl}/image`
  constructor(private _http: HttpClient) {
  }

  public getFileByUrl(url: string){
    return this._http.get(url).toPromise();
  }
  public upload(file: File, key: string){
    const formData = new FormData();
    formData.set('file', file);
    formData.set('key', key);

    return this._http.post<string>(this.imageControllerUri, formData).toPromise();
  }
}

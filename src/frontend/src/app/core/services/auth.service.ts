﻿import {Injectable} from "@angular/core";
import {User} from "../dtos/user.dto";

@Injectable(
  {providedIn: 'root'}
)

export class AuthService{
  tokenKeyword: string = 'token';
  userKeyword: string = 'user';
  constructor() {
  }

  isAuthorized(): boolean{
    return localStorage.getItem(this.tokenKeyword) !== null;
  }

  isAdmin(): boolean{
    return !!localStorage.getItem('isAdmin') && localStorage.getItem('isAdmin') === 'true';
  }

  getToken(): string{
    return localStorage.getItem(this.tokenKeyword)!;
  }

  setUser(user: User, isAdmin: boolean){
    const json = JSON.stringify(user);
    localStorage.setItem(this.userKeyword, json);
    localStorage.setItem('isAdmin', isAdmin.toString());
  }

  setUserData(user: User){
    const json = JSON.stringify(user);
    localStorage.setItem(this.userKeyword, json);
  }

  getUser(): User{
    return JSON.parse(localStorage.getItem(this.userKeyword)!);
  }

  setToken(token: string){
    localStorage.setItem(this.tokenKeyword, token);
  }

  unAuthorize(){
    localStorage.removeItem(this.tokenKeyword);
    localStorage.removeItem(this.userKeyword);
    localStorage.removeItem('isAdmin');
  }
}

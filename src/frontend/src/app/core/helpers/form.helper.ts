﻿import {FormGroup} from "@angular/forms";

export function toFormData(formGroup: FormGroup) {
  const formData = new FormData();

  for (const [key, value] of Object.entries(formGroup.value)) {
    if (typeof value != "object") {
      formData.append(key, String(value))
    }
  }

  return formData;
}

﻿export function formatExceptionMessage(err: any){
  return `Произошла непредвиденная ошибка. ${err.error.Message}. Статус ${err.error.Status}`
}

﻿export function isDarkTheme(){
  return localStorage.getItem('theme') === 'dark-theme';
}

export function getEditorTheme(){
  return isDarkTheme() ? 'vs-dark' : 'vs-light';
}

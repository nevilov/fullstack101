﻿const mockedData = [
  {
    name: '.vscode',
    children: [{name: 'settings.json'}],
  },
  {
    name: 'src1',
    children: [
      {
        name: 'app',
        children: [
          {name: 'app.component.html'},
          {name: 'app.component.css'},
          {name: 'app.component.spec.ts'},
          {name: 'app.component.ts'},
          {name: 'app.module.ts'},
        ],
      },
      {
        name: 'assets',
        children: [{name: '.gitkeep'}],
      },
      {
        name: 'environments',
        children: [
          {name: 'environment.prod.ts'},
          {name: 'environment.ts'},
        ],
      },
      {
        name: 'favicon.ico',
      },
      {
        name: 'index.html',
      },
      {
        name: 'main.ts',
      },
      {
        name: 'polyfill.ts',
      },
      {
        name: 'styles.css',
      },
    ],
  },
  {
    name: 'angular.json',
  },
  {
    name: 'package-lock.json',
  },
  {
    name: 'package.json',
    language: 'json',
    content: '[\n' +
      '    {\n' +
      '        "currency": "GBP",\n' +
      '        "strategy": "FiveTenthPoint"\n' +
      '    },\n' +
      '    {\n' +
      '        "currency": "EUR",\n' +
      '        "strategy": "Point"\n' +
      '    },\n' +
      '    {\n' +
      '        "currency": "CAD",\n' +
      '        "strategy": "OneTenthPoint"\n' +
      '    },\n' +
      '    {\n' +
      '        "currency": "AUD",\n' +
      '        "strategy": "OneTenthPoint"\n' +
      '    },\n' +
      '    {\n' +
      '        "currency": "NOK",\n' +
      '        "strategy": "Point"\n' +
      '    },\n' +
      '    {\n' +
      '        "currency": "HKD",\n' +
      '        "strategy": "Point"\n' +
      '    },\n' +
      '    {\n' +
      '        "currency": "SGD",\n' +
      '        "strategy": "Point"\n' +
      '    }\n' +
      ']'
  },
  {
    name: 'tsconfig.json',
    content: 'function x() {\nconsole.log("Hello world!");\n}',
    language: 'json'
  },
  {
    name: 'src',
    children: [
      {
        name: 'app',
        children: [
          {name: 'app.component.html'},
          {name: 'app.component.css'},
          {name: 'app.component.spec.ts'},
          {name: 'app.component.ts'},
          {name: 'app.module.ts'},
        ],
      },
      {
        name: 'assets',
        children: [{name: '.gitkeep'}],
      },
      {
        name: 'environments',
        children: [
          {name: 'environment.prod.ts'},
          {name: 'environment.ts'},
        ],
      },
      {
        name: 'favicon.ico',
      },
      {
        name: 'index.html',
      },
      {
        name: 'main.ts',
      },
      {
        name: 'polyfill.ts',
      },
      {
        name: 'styles.css',
      },
    ],
  },

]

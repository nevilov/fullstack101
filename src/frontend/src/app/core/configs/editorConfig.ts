﻿export const editorInternalization = {
  messages: {
    ui: {
      "blockTunes": {
        "toggler": {
          "Click to tune": "Нажмите, чтобы настроить",
          "or drag to move": "или перетащите"
        },
      },
      "inlineToolbar": {
        "converter": {
          "Convert to": "Конвертировать в"
        }
      },
      "toolbar": {
        "toolbox": {
          "Add": "Добавить"
        }
      }
    },
    toolNames: {
      "Text": "Параграф",
      "Heading": "Заголовок",
      "List": "Список",
      "Warning": "Примечание",
      "Checklist": "Чеклист",
      "Quote": "Цитата",
      "Code": "Код",
      "Image": "Картинка",
      "Delimiter": "Разделитель",
      "Raw HTML": "HTML-фрагмент",
      "Table": "Таблица",
      "Link": "Ссылка",
      "Marker": "Маркер",
      "Bold": "Полужирный",
      "Italic": "Курсив",
      "InlineCode": "Моноширинный",
    },
    tools: {
      "warning": {
        "Title": "Название",
        "Message": "Сообщение",
      },
      "link": {
        "Add a link": "Вставьте ссылку"
      },
      "stub": {
        'The block can not be displayed correctly.': 'Блок не может быть отображен'
      }
    },
    blockTunes: {
      "delete": {
        "Delete": "Удалить"
      },
      "moveUp": {
        "Move up": "Переместить вверх"
      },
      "moveDown": {
        "Move down": "Переместить вниз"
      }
    },
  }
}

export const defaultMockData = {
  time: 1705226011753,
  blocks: [
    {
      id: "39iCw3xwOs",
      type: "table",
      data: {
        withHeadings: true,
        content: [
          [
            "sad",
            "asd",
            "sd"
          ],
          [
            "sad",
            "ds",
            "sd"
          ],
          [
            "s",
            "sd",
            "ds"
          ]
        ]
      }
    },

  ],
  version: "2.28.2"
}

﻿export interface LoginDto{
  login: string,
  password: string
}

export interface RegisterDto{
  firstName: string,
  lastName: string,
  email: string,
  userName: string,
  password: string,
  birthDate: Date,
}

export interface User{
  firstName: string,
  lastName:string,
  userName: string,
  email: string,
  avatarUrl: string
  birthDate?: Date,
}

export interface PasswordDto{
  oldPassword: string;
  newPassword: string;
  retryPassword: string;
}

export interface AuthDto{
  token: string,
  user: User,
  isAdmin: boolean
}

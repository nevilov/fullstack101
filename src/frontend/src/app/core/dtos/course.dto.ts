﻿import {ShortCourseItemDto} from "./courseItem.dto";

export default interface Course{
  id: string,
  name: string,
  descriptionJson: string,
  slug:string,
  imageUrl: string
  slightDescription: string,
  price: number,
  items: ShortCourseItemDto[]
}

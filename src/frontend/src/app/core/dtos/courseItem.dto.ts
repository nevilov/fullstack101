﻿import {ProjectDto} from "./project.dto";

export interface CourseItemDto{
  id: string,
  order: number,
  title: string,
  contentJson?: string,
  project?: ProjectDto,
  courseId: string,
  itemType: ItemType
}

export interface SaveCourseItemDto {
  order: number,
  title: string,
  contentJson?: string,
  projectId?: string,
  courseId: string,
  itemType: ItemType
}

export interface ShortCourseItemDto{
  id: string,
  title: string,
  order: number
}

export enum ItemType{
  Page = 0,
  Code = 1
}

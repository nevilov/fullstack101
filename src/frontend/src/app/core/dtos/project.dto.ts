﻿export interface ProjectDto{
  id: string,
  title: string,
  description: string,
  jsonTree: string,
  platform: Platform,
  command: string,
  arguments: string
  createdByUserName?: string
}

export interface SaveProjectDto{
  id?: string,
  title: string,
  description: string,
  jsonTree: string,
  platformName: string,
  command: string,
  arguments: string
}

export interface Platform{
  id: number,
  name: string,
  version: string,
  imageName: string,
  description: string
}

﻿export interface ContainerDto{
  id: string,
  name: string,
  port: string,
  lifeTimeMinute: number,
  launchedAt: Date,
  buildOutput: string,
  status: ContainerStatus,
  projectId: string,
  lastContainerLogs: string,
  isInProgress: boolean
}

export enum ContainerStatus{
  InQueue,
  ImageCreationInQueue,
  ImageCreationFailed,
  ContainerCreationInQueue,
  ContainerCreationFailed,
  ProgramFailed,
  Running,
  Terminated,
  Killed
}

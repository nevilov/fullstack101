﻿export const environment = {
  apiUrl: (window as any)["env"]["apiUrl"] || 'https://localhost:7146/api',
  orchestratorApiUrl: (window as any)["env"]["orchestratorApiUrl"] || 'https://localhost:7024/api'
}

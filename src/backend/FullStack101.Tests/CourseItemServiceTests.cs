﻿namespace FullStack101.Tests;

public class CourseItemServiceTests
{
    [Fact]
    public void CreateCourseItem_Success()
    {
        Assert.Equal(1,1);
    }

    [Fact]
    public void GetCourseItems_Success()
    {
        Assert.Equal(1,1);
    }

    [Fact]
    public void Update_Success()
    {
        Assert.Equal(1,1);
    }
}
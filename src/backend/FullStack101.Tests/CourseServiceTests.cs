﻿using FullStack101.Core.Abstractions.Repositories;
using FullStack101.Core.Abstractions.Services;
using FullStack101.Core.Dtos.Course;
using FullStack101.Core.Services;
using FullStack101.Domain.Entities;
using Moq;

namespace FullStack101.Tests;

public class CourseServiceTests
{
    private readonly Mock<ICourseRepository> _courseRepositoryMock;
    private readonly Mock<IS3Service> _s3Mock;

    public CourseServiceTests()
    {
        _courseRepositoryMock = new Mock<ICourseRepository>();
        _s3Mock = new Mock<IS3Service>();
    }

    [Fact]
    public async Task CreateCourse_Success()
    {
        //Arrange
        _courseRepositoryMock.Setup(x => x.Add(It.IsAny<Course>()))
            .Returns(Task.FromResult(""));
        var dto = new SaveCourseDto()
        {
            Id = Guid.NewGuid(),
            Slug = "ss"
        };
        
        //Act
        var service = new CourseService(_s3Mock.Object, _courseRepositoryMock.Object);
        await service.Create(dto);
        
        //Assert
        _courseRepositoryMock.Verify(x => x.Add(It.IsAny<Course>()));
    }
}
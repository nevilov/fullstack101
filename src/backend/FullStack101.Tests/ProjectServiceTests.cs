﻿namespace FullStack101.Tests;

public class ProjectServiceTests
{

    [Fact]
    public void CloneProject_Success()
    {
        Assert.Equal(1,1);
    }

    [Fact]
    public void CreateProject_Success()
    {
        Assert.Equal(1, 1);
    }
}
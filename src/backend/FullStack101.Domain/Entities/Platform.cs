﻿namespace FullStack101.Domain.Entities;

public class Platform
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Version { get; set; }
    public string ImageName { get; set; }
    public string Description { get; set; }
}
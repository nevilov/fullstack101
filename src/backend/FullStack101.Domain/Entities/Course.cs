﻿namespace FullStack101.Domain.Entities;

public class Course : EntityBase
{
    public string Name { get; set; } = null!;
    public string DescriptionJson { get; set; } = null!;
    public string Slug { get; set; } = null!;
    public string CoverImageUrl { get; set; } = null!;
    public ICollection<CourseItem> CourseItems { get; set; }
    
    public decimal? Price { get; set; }
    public string SlightDescription { get; set; } = null!;
}
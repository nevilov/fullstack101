﻿namespace FullStack101.Domain.Entities;

public class CourseItem : EntityBase
{
    public int Order { get; set; }
    
    public string Title { get; set; }
    public string? ContentJson { get; set; }
    public ItemType ItemType { get; set; }
    public Course Course { get; set; }
    public Guid CourseId { get; set; }
    public Project? Project { get; set; }
}

public enum ItemType
{
    Page,
    Code
}
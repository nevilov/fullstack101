﻿namespace FullStack101.Domain.Entities;

public class Project : EntityBase
{
    public string Title { get; set; } = null!;
    public string Description { get; set; } = null!;
    public string JsonTree { get; set; } = null!;
    public Platform Platform { get; set; }
    
    public bool IsOriginal { get; set; }
    public Guid? ParentProjectId { get; set; }
    public int? Port { get; set; }
    public string Command { get; set; }
    public string Arguments { get; set; }
    
    public Guid? CreatedById { get; set; }
    
    public User? CreatedBy { get; set; }
    
    //public CourseItem? RelatedCourseItem { get; set; }
    //public Guid? RelatedCourseItemId { get; set; }

    public Project Clone()
    {
        return new Project()
        {
            Title = Title,
            Arguments = Arguments,
            Command = Command,
            Description = Description,
            Port = Port,
            Platform = Platform,
            JsonTree = JsonTree,
            ParentProjectId = Id
        };
    }
}
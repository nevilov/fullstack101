﻿using Microsoft.AspNetCore.Identity;

namespace FullStack101.Domain.Entities;

public class User : IdentityUser<Guid>
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? AvatarUrl { get; set; }
    public DateTime BirthDate { get; set; }
    public ICollection<Project> Projects { get; set; }
}
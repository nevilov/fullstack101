﻿using FullStack101.Domain.Entities;
using FullStack101.Infrastructure.Data.Configurations;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FullStack101.Infrastructure.Data;

public class ApplicationDbContext : IdentityDbContext<User, Role, Guid>
{
    public DbSet<Course> Courses { get; set; }
    public DbSet<CourseItem> CourseItems { get; set; }
    public DbSet<Project> Projects { get; set; }
    public DbSet<Platform> Platforms { get; set; }

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Course>(e =>
        {
            e.HasKey(x => x.Id);
            e.Property(x => x.Name).IsRequired();
            e.Property(x => x.DescriptionJson).HasColumnType("json").IsRequired(false);
        });

        modelBuilder.Entity<CourseItem>(e =>
        {
            e.HasKey(x => x.Id);
            e.Property(x => x.Title).IsRequired();
            e.Property(x => x.ContentJson).HasColumnType("json").IsRequired(false);
            e.HasOne<Course>(x => x.Course).WithMany(item => item.CourseItems).HasForeignKey(x => x.CourseId);
        });

        modelBuilder.Entity<Project>(e =>
        {
            e.HasKey(x => x.Id);
            e.Property(x => x.JsonTree).HasColumnType("json").IsRequired();
            e.Property(x => x.Title).IsRequired();
            e.Property(x => x.Description).IsRequired(false);
            e.HasOne(x => x.CreatedBy)
                .WithMany(x => x.Projects)
                .HasForeignKey(x => x.CreatedById)
                .IsRequired(false);
        });

        modelBuilder.Entity<User>(e =>
        {
            e.Property(x => x.BirthDate)
                .HasColumnType("date");
        });
        
        modelBuilder.ApplyConfiguration(new RoleConfiguration());
        base.OnModelCreating(modelBuilder);
    }
}
﻿using FullStack101.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FullStack101.Infrastructure.Data.Configurations;

public class RoleConfiguration : IEntityTypeConfiguration<Role>
{
    public void Configure(EntityTypeBuilder<Role> builder)
    {
        builder.HasData(
            new Role
            {
                Id = Guid.Parse("5cf0a502-2d41-4eb0-a3a9-93d52c1169de"),
                Name = "User",
                NormalizedName = "USER"
            },
            new Role
            {
                Id = Guid.Parse("4c908cb7-eb0c-41fc-8c28-04360a349c52"),
                Name = "Administrator",
                NormalizedName = "ADMINISTRATOR"
            });
        
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace FullStack101.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class Additemtype : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("8f01df85-cddd-41a1-a872-26c5bb7ce675"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("99896f52-7af6-4cd6-96bd-57d90b4c46fe"));

            migrationBuilder.AddColumn<int>(
                name: "ItemType",
                table: "CourseItems",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "ProjectId",
                table: "CourseItems",
                type: "uuid",
                nullable: true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("4c908cb7-eb0c-41fc-8c28-04360a349c52"), null, "Administrator", "ADMINISTRATOR" },
                    { new Guid("5cf0a502-2d41-4eb0-a3a9-93d52c1169de"), null, "User", "USER" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CourseItems_ProjectId",
                table: "CourseItems",
                column: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_CourseItems_Projects_ProjectId",
                table: "CourseItems",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CourseItems_Projects_ProjectId",
                table: "CourseItems");

            migrationBuilder.DropIndex(
                name: "IX_CourseItems_ProjectId",
                table: "CourseItems");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("4c908cb7-eb0c-41fc-8c28-04360a349c52"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("5cf0a502-2d41-4eb0-a3a9-93d52c1169de"));

            migrationBuilder.DropColumn(
                name: "ItemType",
                table: "CourseItems");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "CourseItems");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("8f01df85-cddd-41a1-a872-26c5bb7ce675"), null, "User", "USER" },
                    { new Guid("99896f52-7af6-4cd6-96bd-57d90b4c46fe"), null, "Administrator", "ADMINISTRATOR" }
                });
        }
    }
}

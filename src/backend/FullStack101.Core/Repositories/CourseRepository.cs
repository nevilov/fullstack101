﻿using FullStack101.Core.Abstractions.Repositories;
using FullStack101.Domain.Entities;
using FullStack101.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace FullStack101.Core.Repositories;

public class CourseRepository(ApplicationDbContext dbContext) : ICourseRepository
{
    public async Task SaveChanges()
    {
        await dbContext.SaveChangesAsync();
    }

    public async Task<IEnumerable<Course>> GetCourses()
    {
        return await dbContext.Courses
            .Include(x => x.CourseItems)
            .ToListAsync();
    }

    public async Task Add(Course course)
    {
        await dbContext.Courses.AddAsync(course);
        await SaveChanges();
    }

    public async Task<Course?> GetBySlug(string slug)
    {
        return await dbContext.Courses
            .Include(x => x.CourseItems)
            .FirstOrDefaultAsync(x => x.Slug == slug);
    }

    public async Task<Course?> GetById(Guid id)
    {
       return await dbContext.Courses
           .Include(x => x.CourseItems)
           .FirstOrDefaultAsync(x => x.Id == id);
    }
}
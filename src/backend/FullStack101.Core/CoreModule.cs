﻿using Amazon.Runtime;
using Amazon.S3;
using FullStack101.Core.Abstractions.Helpers;
using FullStack101.Core.Abstractions.Repositories;
using FullStack101.Core.Abstractions.Services;
using FullStack101.Core.Configs;
using FullStack101.Core.Helpers;
using FullStack101.Core.HttpClients;
using FullStack101.Core.Repositories;
using FullStack101.Core.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FullStack101.Core;

public static class CoreModule
{
    public static IServiceCollection AddCoreModule(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddHttpContextAccessor();

        services
            .AddScoped<UserService>()
            .AddScoped<ImageService>()
            .AddScoped<CourseService>()
            .AddScoped<CourseItemService>()
            .AddScoped<ProjectService>();


        services.AddScoped<ICourseRepository, CourseRepository>();
        services
            .AddScoped<TokenHelper>()
            .AddScoped<ICurrentUserAccessor, CurrentUserAccessor>();

        services.AddHttpClient<OrchestratorHttpClient>(resolver =>
        {
            resolver.BaseAddress = new Uri(configuration["OrchestratorUrl"]!);
        });

        AddS3(services, configuration);
        return services;
    }

    private static IServiceCollection AddS3(IServiceCollection services, IConfiguration configuration)
    {
        const string s3SectionName = "S3";
        var options = configuration.GetAWSOptions(s3SectionName);
            
        options.Credentials =
            new BasicAWSCredentials(
                configuration[$"{s3SectionName}:AWSAccessKey"],
                configuration[$"{s3SectionName}:AWSSecretKey"]
            );
            
        services.AddAWSService<IAmazonS3>(options);
        services.AddScoped<IS3Service, S3Service>();

        var s3Config = configuration
            .GetRequiredSection(s3SectionName)
            .Get<AwsS3Config>()!;
        services.AddSingleton(s3Config);
            
        return services;
    }
}
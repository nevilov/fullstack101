﻿using FullStack101.Core.Dtos.Project;

namespace FullStack101.Core.Helpers;

public class ProjectHelper
{
    public static List<ProjectTreeDto> GetFlatFileNodes(ProjectTreeDto[] tree, string pathPrefix = "")
    {
        var resultNodes = new List<ProjectTreeDto>();
        foreach (var node in tree)
        {
            var updatedPrefix = string.IsNullOrEmpty(pathPrefix) ? node.Name : $"{pathPrefix}/{node.Name}";
            node.Name = updatedPrefix;
            
            if (node.Children is not null)
            {
                resultNodes.AddRange(GetFlatFileNodes(node.Children, updatedPrefix));
            }
            else
            {
                resultNodes.Add(node);
            }

        }

        return resultNodes;
    }
}
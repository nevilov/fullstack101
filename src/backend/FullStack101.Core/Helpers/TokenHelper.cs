﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using FullStack101.Core.Configs;
using FullStack101.Core.Constants;
using FullStack101.Domain.Entities;
using Microsoft.IdentityModel.Tokens;

namespace FullStack101.Core.Helpers;

public class TokenHelper
{
    private readonly JwtConfig _jwtConfig;

    public TokenHelper(JwtConfig jwtConfig)
    {
        _jwtConfig = jwtConfig;
    }

    public string GenerateToken(User user, string role)
    {
        return GenerateToken(user.UserName!, user.Email!, role);
    }

    public string GenerateToken(string userName, string email, string userRole)
    {
        var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtConfig.SecretKey));    
        var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);    
        
        var token = new JwtSecurityToken(
            _jwtConfig.Issuer,
            _jwtConfig.Issuer,    
            new[]
            {
                new Claim(ApplicationClaimTypes.UserName, userName),
                new Claim(ApplicationClaimTypes.Email, email),
                new Claim(ApplicationClaimTypes.Role, userRole)
            },    
            expires: DateTime.Now.AddMinutes(_jwtConfig.ExpireInMinutes),    
            signingCredentials: credentials);

        return new JwtSecurityTokenHandler().WriteToken(token);
    }
}
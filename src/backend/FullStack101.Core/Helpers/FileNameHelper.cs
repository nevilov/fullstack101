﻿namespace FullStack101.Core.Helpers;

public class FileNameHelper
{
    public static string GenerateFilePath(string subFolder, string fileName, string extension)
        => $"{subFolder.TrimEnd('/')}/{GenerateFileName(fileName)}.{extension.TrimStart('.')}";
    public static string GenerateFileName(string fileName, string prefix = "")
    {
        fileName = fileName
            .Trim()
            .Trim('/')
            .Replace(" ", "_");

        var hash = Guid.NewGuid().ToString()[..4];
        return $"{prefix}{fileName}_{hash}";
    }
    public static string GenerateKey(string subFolder, string fileName, string extension)
        => $"{subFolder.TrimEnd('/')}/{fileName}.{extension.TrimStart('.')}";
}
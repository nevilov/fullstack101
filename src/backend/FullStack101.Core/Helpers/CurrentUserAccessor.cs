﻿using System.Security.Claims;
using FullStack101.Core.Abstractions.Helpers;
using FullStack101.Core.Constants;
using FullStack101.Core.Exceptions;
using Microsoft.AspNetCore.Http;

namespace FullStack101.Core.Helpers;

public class CurrentUserAccessor : ICurrentUserAccessor
{
    private readonly IHttpContextAccessor _contextAccessor;

    public CurrentUserAccessor(IHttpContextAccessor contextAccessor)
    {
        _contextAccessor = contextAccessor;
    }

    public string GetUserName()
    {
        var userName = _contextAccessor?.HttpContext?.User.FindFirstValue(ApplicationClaimTypes.UserName);

        if (string.IsNullOrEmpty(userName))
        {
            throw new NotFoundException("User were not found");
        }

        return userName;
    }
}
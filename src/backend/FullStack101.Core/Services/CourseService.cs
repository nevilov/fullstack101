﻿using FullStack101.Core.Abstractions.Repositories;
using FullStack101.Core.Abstractions.Services;
using FullStack101.Core.Dtos.Course;
using FullStack101.Core.Exceptions;
using FullStack101.Core.Helpers;
using FullStack101.Core.Mappers;
using FullStack101.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace FullStack101.Core.Services;

public class CourseService
{
    private readonly ICourseRepository _courseRepository;
    private readonly IS3Service _s3Service;

    public CourseService(IS3Service s3Service, ICourseRepository courseRepository)
    {
        _s3Service = s3Service;
        _courseRepository = courseRepository;
    }

    public async Task<IEnumerable<CourseDto>> Get()
    {
        var courses = await _courseRepository.GetCourses();
        return courses.Select(CourseMapper.ToDto);
    }

    public async Task Create(SaveCourseDto dto)
    {
        var entity = dto.ToEntity();

        if (dto.CoverImageFile is not null)
        {
            entity.CoverImageUrl = await UploadImage(dto);
        }

        await _courseRepository.Add(entity);
    }

    private async Task<string> UploadImage(SaveCourseDto courseDto)
    {
        var fileExtension = Path.GetExtension(courseDto.CoverImageFile!.FileName);
        var fullFilePath = FileNameHelper.GenerateFilePath($"courses/{courseDto.Id}", courseDto.Name, fileExtension);
        var uploadedImageUrl = await _s3Service.CreateObject(courseDto.CoverImageFile!, fullFilePath);
        return uploadedImageUrl;
    }

    public async Task<CourseDto> GetBySlug(string slug)
    {
        var course = await _courseRepository.GetBySlug(slug);

        if (course is null)
        {
            throw new NotFoundException($"A course with slug = {slug} not found");
        }

        return CourseMapper.ToDto(course);
    }

    public async Task Update(SaveCourseDto dto)
    {
        var existingCourse = await _courseRepository.GetById(dto.Id);
        
        if (existingCourse is null)
        {
            throw new NotFoundException($"A course with id = {dto.Id} not found");
        }

        existingCourse.Name = dto.Name;
        existingCourse.DescriptionJson = dto.DescriptionJson;
        existingCourse.SlightDescription = dto.SlightDescription;
        existingCourse.Slug = dto.Slug;
        existingCourse.Price = dto.Price;
        existingCourse.UpdatedAt = DateTime.UtcNow;

        if (dto.CoverImageFile is not null)
        {
            //TODO: remove existing image
            existingCourse.CoverImageUrl = await UploadImage(dto);
        }

        await _courseRepository.SaveChanges();
    }

    public async Task UpdateItemsOrder(Guid id, IEnumerable<ShortCourseItemDto> courseItemDtos)
    {
        var course = await _courseRepository.GetById(id);

        foreach (var courseItem in courseItemDtos)
        {
            var existingCourseItem = course.CourseItems.First(x => x.Id == courseItem.Id);
            existingCourseItem.Order = courseItem.Order;
        }

        await _courseRepository.SaveChanges();
    }
}
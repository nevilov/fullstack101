﻿using FullStack101.Core.Abstractions.Services;
using FullStack101.Core.Dtos.Image;
using FullStack101.Core.Helpers;

namespace FullStack101.Core.Services;

public class ImageService
{
    private readonly IS3Service _s3Service;

    public ImageService(IS3Service s3Service)
    {
        _s3Service = s3Service;
    }

    public async Task<string> Upload(ImageDto imageDto)
    {
        var fileExtension = Path.GetExtension(imageDto.File.FileName);
        var fileName = Path.GetFileNameWithoutExtension(imageDto.File.FileName);
        var fullFilePath = FileNameHelper.GenerateFilePath(imageDto.Key, fileName, fileExtension);
        return await _s3Service.CreateObject(imageDto.File, fullFilePath);
    }
}
using Amazon.S3;
using Amazon.S3.Model;
using FullStack101.Core.Abstractions.Services;
using FullStack101.Core.Configs;
using FullStack101.Core.Dtos.S3;
using Microsoft.AspNetCore.Http;

namespace FullStack101.Core.Services
{
    public class S3Service : IS3Service
    {
        private readonly IAmazonS3 _s3;
        private readonly AwsS3Config _awsConfig;
        public S3Service(IAmazonS3 s3, AwsS3Config awsConfig)
        {
            _s3 = s3;
            _awsConfig = awsConfig;
        }

        public string GetObjectUrl(string key) => $"{_awsConfig.CustomServiceUrl.Trim('/')}/{key}";
        
        public async Task<string> CreateObject(IFormFile file, string filePath)
        {
            await using var newMemoryStream = new MemoryStream();
            await file.CopyToAsync(newMemoryStream);

            await _s3.PutObjectAsync(new PutObjectRequest
            {
                Key = filePath,
                BucketName = _awsConfig.BucketName,
                CannedACL = S3CannedACL.PublicRead,
                InputStream = newMemoryStream
            });
            
            return GetObjectUrl(filePath);
        }

        public async Task CreateObject(MemoryStream stream, string key)
        {
            await _s3.PutObjectAsync(new PutObjectRequest
            {
                Key = key,
                BucketName = _awsConfig.BucketName,
                CannedACL = S3CannedACL.PublicRead,
                InputStream = stream,
            });
        }

        public async Task CreateObject(byte[] byteArray, string key)
        {
            using var ms = new MemoryStream(byteArray);
            await _s3.PutObjectAsync(new PutObjectRequest
            {
                Key = key,
                BucketName = _awsConfig.BucketName,
                CannedACL = S3CannedACL.PublicRead,
                InputStream = ms,
            });
        }

        public async Task CreateObject(S3ObjectRequest obj)
        {
            await _s3.PutObjectAsync(new PutObjectRequest
            {
                Key = obj.Key,
                BucketName = _awsConfig.BucketName,
                CannedACL = S3CannedACL.PublicRead,
                ContentBody = obj.Content,
            });
        }

        public async Task<IList<string>> ListBucketContent(string prefix)
        {
            var request = new ListObjectsV2Request()
            {
                BucketName = _awsConfig.BucketName,
                Prefix = prefix
            };

            var listObjects = await _s3.ListObjectsV2Async(request);
            return listObjects.S3Objects.Select(x => x.Key).ToList();
        }
    }
}
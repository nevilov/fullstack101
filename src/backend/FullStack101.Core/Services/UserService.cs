﻿using FullStack101.Core.Abstractions.Helpers;
using FullStack101.Core.Abstractions.Services;
using FullStack101.Core.Constants;
using FullStack101.Core.Dtos.User;
using FullStack101.Core.Exceptions;
using FullStack101.Core.Helpers;
using FullStack101.Core.Mappers;
using FullStack101.Domain.Entities;
using FullStack101.Infrastructure.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace FullStack101.Core.Services;

public class UserService
{
    private readonly ApplicationDbContext _dbContext;
    private readonly UserManager<User> _userManager;
    private readonly TokenHelper _tokenHelper;
    private readonly ILogger _logger;
    private readonly IS3Service _s3Service;
    private readonly ICurrentUserAccessor _currentUserAccessor;

    public UserService(ApplicationDbContext dbContext, UserManager<User> userManager, ILogger logger, TokenHelper tokenHelper, IS3Service s3Service, ICurrentUserAccessor currentUserAccessor)
    {
        _dbContext = dbContext;
        _userManager = userManager;
        _logger = logger;
        _tokenHelper = tokenHelper;
        _s3Service = s3Service;
        _currentUserAccessor = currentUserAccessor;
    }

    public async Task Update(UpdateUserDto dto)
    {
        var user = await GetAuthorizedUser();

        if (dto.Avatar is not null)
        {
            user.AvatarUrl = await UploadImage(user.UserName!, dto.Avatar);
        }

        user.FirstName = dto.FirstName;
        user.LastName = dto.LastName;
        user.Email = dto.Email;
        user.BirthDate = dto.BirthDate;
        
        await _dbContext.SaveChangesAsync();
    }

    public async Task<AuthResponseDto> Login(LoginUserDto dto)
    {
        var user = await _userManager.Users
            .Where(x => x.UserName == dto.Login || x.Email == dto.Login)
            .FirstOrDefaultAsync();
        
        if (user is null || !await _userManager.CheckPasswordAsync(user, dto.Password))
        {
            throw new AuthException("Login or password is incorrect");
        }

        var userRole = await _userManager.IsInRoleAsync(user, ApplicationRoles.Administrator)
            ? ApplicationRoles.Administrator
            : ApplicationRoles.User;
        
        return new AuthResponseDto()
        {
            Token = _tokenHelper.GenerateToken(user, userRole),
            User = UserMapper.ToDto(user),
            IsAdmin = userRole is ApplicationRoles.Administrator
        };
    }

    private async Task<User> GetAuthorizedUser()
    {
        var user = await _dbContext.Users
            .Where(x => x.UserName == _currentUserAccessor.GetUserName())
            .FirstOrDefaultAsync();

        return user ?? throw new AuthException("Non authorized");
    }

    public async Task<UserDto> GetCurrentUser()
    {
        return UserMapper.ToDto(await GetAuthorizedUser());
    }
       
    public async Task<AuthResponseDto> Register(CreateUserDto userDto)
    {
        var user = userDto.ToUserEntity();
        if (userDto.Avatar is not null)
        {
            user.AvatarUrl = await UploadImage(user.UserName!, userDto.Avatar);
        }
        
        var result = await _userManager.CreateAsync(user, userDto.Password);

        if (!result.Succeeded)
        {
            throw new AuthException($"An exception occured on registration. {string.Join(", ", result.Errors.Select(x => x.Description))}");
        }

        _logger.Information("New User successfully created. User = {user}", user);
        await _dbContext.SaveChangesAsync();
        
        var userRole = await _userManager.IsInRoleAsync(user, ApplicationRoles.Administrator)
            ? ApplicationRoles.Administrator
            : ApplicationRoles.User;
        return new AuthResponseDto()
        {
            Token = _tokenHelper.GenerateToken(user, userRole),
            User = UserMapper.ToDto(user),
            IsAdmin = userRole is ApplicationRoles.Administrator
        };
    }

    private async Task<string> UploadImage(string key, IFormFile img)
    {
        var extension = Path.GetExtension(img.FileName);
        var imageKey = FileNameHelper.GenerateKey("users", key, extension);
        return await _s3Service.CreateObject(img, imageKey);
    }

    public async Task UpdatePassword(UpdatePasswordDto dto)
    {
        var user = await GetAuthorizedUser();
        
        if (!(await _userManager.CheckPasswordAsync(user, dto.OldPassword)))
        {
            throw new AuthException("Login or password is incorrect");
        }

        user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, dto.NewPassword);
        await _dbContext.SaveChangesAsync();
    }
}
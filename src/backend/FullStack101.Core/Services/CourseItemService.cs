﻿using FullStack101.Core.Dtos.CourseItem;
using FullStack101.Core.Exceptions;
using FullStack101.Core.Mappers;
using FullStack101.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace FullStack101.Core.Services;

public class CourseItemService
{
    private readonly ApplicationDbContext _dbContext;

    public CourseItemService(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task Create(SaveCourseItemDto dto)
    {
        var course = _dbContext.Courses.FirstOrDefault(x => x.Id == dto.CourseId);
        if (course is null)
        {
            throw new NotFoundException($"A course with id {dto.CourseId} was not found");
        }
        
        var entity = CourseItemMapper.ToEntity(dto);
        await _dbContext.CourseItems.AddAsync(entity);
        await _dbContext.SaveChangesAsync();
    }

    public async Task Update(Guid id, SaveCourseItemDto dto)
    {
        var courseItem = await _dbContext.CourseItems
            .Include(x => x.Project)
            .FirstOrDefaultAsync(x => x.Id == id);

        if (courseItem is null)
        {
            throw new NotFoundException($"A course item with id {id} was not found");
        }
        
        var course = _dbContext.Courses.FirstOrDefault(x => x.Id == dto.CourseId);
        if (course is null)
        {
            throw new NotFoundException($"A course with id {dto.CourseId} was not found");
        }
        
        var project = dto.ProjectId is not null
            ? await _dbContext.Projects.FirstAsync(x => x.Id == dto.ProjectId)
            : null;

        courseItem.CourseId = dto.CourseId;
        courseItem.ItemType = dto.ItemType;
        courseItem.Order = dto.Order;
        courseItem.Title = dto.Title;
        courseItem.ContentJson = dto.ContentJson;
        courseItem.UpdatedAt = DateTime.UtcNow;
        courseItem.Project = project;

        await _dbContext.SaveChangesAsync();
    }
        
    public async Task<CourseItemDto> Get(Guid id)
    {
        var item = await _dbContext.CourseItems
            .Include(x => x.Project)
            .FirstAsync(x => x.Id == id);
        return CourseItemMapper.ToDto(item);
    }
}
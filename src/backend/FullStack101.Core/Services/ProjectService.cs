﻿using System.IO.Compression;
using System.Text.Json;
using FullStack101.Core.Abstractions.Helpers;
using FullStack101.Core.Abstractions.Services;
using FullStack101.Core.Dtos.Project;
using FullStack101.Core.Dtos.S3;
using FullStack101.Core.Exceptions;
using FullStack101.Core.Helpers;
using FullStack101.Core.HttpClients;
using FullStack101.Core.HttpClients.Dtos;
using FullStack101.Core.Mappers;
using FullStack101.Domain.Entities;
using FullStack101.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace FullStack101.Core.Services;

public class ProjectService
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IS3Service _s3;
    private readonly OrchestratorHttpClient _orchestratorHttpClient;
    private readonly ICurrentUserAccessor _currentUserAccessor;

    public ProjectService(ApplicationDbContext dbContext, IS3Service s3, OrchestratorHttpClient orchestratorHttpClient, ICurrentUserAccessor currentUserAccessor)
    {
        _dbContext = dbContext;
        _s3 = s3;
        _orchestratorHttpClient = orchestratorHttpClient;
        _currentUserAccessor = currentUserAccessor;
    }

    public async Task<ProjectDto> GetProject(Guid id)
    {
        var entity = await _dbContext.Projects
            .Include(x => x.Platform)
            .Include(x => x.CreatedBy)
            .FirstOrDefaultAsync(x => x.Id == id);
        
        if (entity is null)
        {
            throw new NotFoundException($"An project with id = {id} was not found");
        }

        return ProjectMapper.ToDto(entity);
    }

    public async Task<Guid> Save(SaveProjectDto dto)
    {
        if (dto.Id is null)
        {
            return await Create(dto);
        }

        return await Update(dto);
    }

    public async Task<Guid> Create(SaveProjectDto dto)
    {
        var platform = await _dbContext.Platforms.FirstOrDefaultAsync(x => x.Name == dto.PlatformName);
        if (platform is null)
        {
            throw new NotFoundException($"An platform {dto.PlatformName} was not found");
        }
        
        var user = await _dbContext.Users.FirstAsync(x => x.UserName == _currentUserAccessor.GetUserName());
        var entity = ProjectMapper.ToEntity(dto);
        entity.Port = dto.OccupyPort ? await GetFreePort() : null;
        entity.Platform = platform;
        entity.CreatedBy = user;
        
        await _dbContext.Projects.AddAsync(entity);
        await _dbContext.SaveChangesAsync();

        var zipFileKey = $"projects/{entity.Id}.zip";
        await UploadFileTreeToS3Zip(entity.JsonTree, zipFileKey);

        return entity.Id;
    }

    public async Task<Guid> Update(SaveProjectDto dto)
    {
        var platform = await _dbContext.Platforms.FirstOrDefaultAsync(x => x.Name == dto.PlatformName);
        if (platform is null)
        {
            throw new NotFoundException($"An platform {dto.PlatformName} was not found");
        }
        var project = await _dbContext.Projects.FirstAsync(x => x.Id == dto.Id)!;
        
        project.Platform = platform;
        project.Arguments = dto.Arguments;
        project.Command = dto.Command;
        project.Title = dto.Title;
        project.Description = dto.Description;
        project.JsonTree = dto.JsonTree;
        project.UpdatedAt = DateTime.UtcNow;

        await _dbContext.SaveChangesAsync();
        
        var zipFileKey = $"projects/{project.Id}.zip";
        await UploadFileTreeToS3Zip(project.JsonTree, zipFileKey);
        
        return project.Id;
    }

    private async Task<int> GetFreePort()
    {
        return await _dbContext.Projects.MaxAsync(x => x.Port) + 1 ?? 1000;
    }

    public async Task Run(Guid projectId)
    {
        var project = await _dbContext.Projects
            .Include(x => x.Platform)
            .FirstAsync(x => x.Id == projectId);

        var port = project.Port is not null ? $"{project.Port}:{project.Port}" : string.Empty;
        
        var requestDto = new CreateContainerRequestDto()
        {
            ImageName = project.Platform.ImageName,
            Arguments = project.Arguments,
            Command = project.Command,
            ProjectId = projectId,
            Port = port,
            Name = projectId.ToString(),
            Background = true,
            Interactive = true,
        };
        
        await Task.Run(async () => await _orchestratorHttpClient.RunContainer(requestDto));
    }
    
    private async Task UploadFileTreeToS3Zip(string treeJson, string key)
    {
        var projectTree = JsonSerializer.Deserialize<ProjectTreeDto[]>(treeJson);
        if (projectTree is null)
        {
            return;
        }
        
        var flatNodes = ProjectHelper.GetFlatFileNodes(projectTree)
            .Where(x => !string.IsNullOrEmpty(x.Content))
            .ToList();

        using var memoryStream = new MemoryStream();
        using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
        {
            flatNodes.ForEach(node =>
            {
                var file = archive.CreateEntry(node.Name, CompressionLevel.Fastest);
                Console.WriteLine(file.Length);
                using var entryStream = file.Open();
                using var streamWriter = new StreamWriter(entryStream);
                streamWriter.Write(node.Content);
                streamWriter.Close();
            });
        }
        
        memoryStream.Position = 0;
        await _s3.CreateObject(memoryStream, key);
    }
    
    public async Task<IEnumerable<ProjectDto>> Get(ProjectQueryRequestDto dto)
    {
        var projects = _dbContext.Projects
            .Include(x => x.Platform)
            .Include(x => x.CreatedBy)
            .AsQueryable();

        if (dto.FilterByUser)
        {
            var currentUser = _currentUserAccessor.GetUserName();
            projects = projects.Where(x => x.CreatedBy != null && x.CreatedBy.UserName == currentUser);
        }

        return (await projects.ToListAsync()).Select(ProjectMapper.ToDto);
    }

    public async Task<IEnumerable<Platform>> GetPlatforms()
    {
        return await _dbContext.Platforms
            .ToListAsync();
    }

    public async Task Delete(Guid id)
    {
        var project = await _dbContext.Projects.FirstAsync(x => x.Id == id);
        _dbContext.Projects.Remove(project);
        await _dbContext.SaveChangesAsync();
    }

    public async Task<Guid> Clone(Guid projectId)
    {
        var project = await _dbContext.Projects
            .Include(x => x.Platform)
            .FirstAsync(x => x.Id == projectId);
        var user = await _dbContext.Users.FirstAsync(x => x.UserName == _currentUserAccessor.GetUserName());

        var cloneProject = project.Clone();
        cloneProject.CreatedBy = user;
        cloneProject.Port = await GetFreePort(); //TODO THink

        await _dbContext.Projects.AddAsync(cloneProject);
        await _dbContext.SaveChangesAsync();
        
        var zipFileKey = $"projects/{cloneProject.Id}.zip";
        await UploadFileTreeToS3Zip(cloneProject.JsonTree, zipFileKey);

        return cloneProject.Id;
    }
}
﻿using FullStack101.Core.Dtos.Course;
using FullStack101.Domain.Entities;

namespace FullStack101.Core.Mappers;

public static class CourseMapper
{
    public static CourseDto ToDto(Course course) => new ()
    {
        Id = course.Id,
        Name = course.Name,
        DescriptionJson = course.DescriptionJson,
        Slug = course.Slug,
        ImageUrl = course.CoverImageUrl,
        SlightDescription = course.SlightDescription,
        Price = course.Price,
        Items = course.CourseItems.Select(x => new ShortCourseItemDto(x.Id, x.Title, x.Order))
    };
}
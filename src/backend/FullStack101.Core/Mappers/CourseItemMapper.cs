﻿using FullStack101.Core.Dtos.CourseItem;
using FullStack101.Core.Dtos.Project;
using FullStack101.Domain.Entities;

namespace FullStack101.Core.Mappers;

public static class CourseItemMapper
{
    public static CourseItem ToEntity(CourseItemDto dto)
    {
        return new CourseItem()
        {
            Id = dto.Id,
            ContentJson = dto.ContentJson,
            CourseId = dto.CourseId,
            Title = dto.Title,
            Order = dto.Order,
            ItemType = dto.ItemType
        };
    }

    public static CourseItem ToEntity(SaveCourseItemDto dto)
    {
        return new CourseItem()
        {
            ContentJson = dto.ContentJson,
            CourseId = dto.CourseId,
            Title = dto.Title,
            Order = dto.Order,
            ItemType = dto.ItemType
        };
    }

    public static CourseItemDto ToDto(CourseItem courseItem) => new CourseItemDto()
    {
        Id = courseItem.Id,
        ContentJson = courseItem.ContentJson,
        CourseId = courseItem.CourseId,
        ItemType = courseItem.ItemType,
        Order = courseItem.Order,
        Project = courseItem.Project is null ? null : ProjectMapper.ToDto(courseItem.Project),
        Title = courseItem.Title
    };
}
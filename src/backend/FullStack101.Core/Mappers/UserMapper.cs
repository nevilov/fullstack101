﻿using FullStack101.Core.Dtos.User;
using FullStack101.Domain.Entities;

namespace FullStack101.Core.Mappers;

public class UserMapper
{
    public static UserDto ToDto(User user) => new UserDto()
    {
        UserName = user.UserName!,
        AvatarUrl = user.AvatarUrl,
        FirstName = user.FirstName,
        LastName = user.LastName,
        Email = user.Email!,
        BirthDate = user.BirthDate
    };
}
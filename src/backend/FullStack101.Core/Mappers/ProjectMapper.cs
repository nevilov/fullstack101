﻿using FullStack101.Core.Dtos.Project;
using FullStack101.Domain.Entities;

namespace FullStack101.Core.Mappers;

public static class ProjectMapper
{
    public static ProjectDto ToDto(Project project)
    {
        return new ProjectDto()
        {
            Id = project.Id,
            Description = project.Description,
            Title = project.Title,
            JsonTree = project.JsonTree,
            Platform = project.Platform,
            Command = project.Command,
            Arguments = project.Arguments,
            CreatedByUserName = project.CreatedBy?.UserName
        };
    }

    public static Project ToEntity(SaveProjectDto dto)
    {
        return new Project()
        {
            Description = dto.Description,
            Title = dto.Title,
            JsonTree = dto.JsonTree,
            ParentProjectId = dto.ReferencedProjectId,
            Command = dto.Command,
            Arguments = dto.Arguments,
        };
    }
}
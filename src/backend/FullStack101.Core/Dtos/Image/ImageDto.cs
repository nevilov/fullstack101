﻿using Microsoft.AspNetCore.Http;

namespace FullStack101.Core.Dtos.Image;

public class ImageDto
{
    public IFormFile File { get; set; }
    public string Key { get; set; }
}
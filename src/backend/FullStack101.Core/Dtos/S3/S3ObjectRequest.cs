﻿namespace FullStack101.Core.Dtos.S3;

public class S3ObjectRequest
{
    public string Content { get; set; }
    public string Key { get; set; }
    public string ContentType { get; set; }
}
﻿using FullStack101.Domain.Entities;

namespace FullStack101.Core.Dtos.CourseItem;

public class SaveCourseItemDto
{
    public int Order { get; set; }
    public ItemType ItemType { get; set; }
    public string Title { get; set; }
    public string ContentJson { get; set; }
    public Guid? ProjectId { get; set; }
    
    public Guid CourseId { get; set; }
}
﻿using FullStack101.Core.Dtos.Project;
using FullStack101.Domain.Entities;

namespace FullStack101.Core.Dtos.CourseItem;

public class CourseItemDto
{
    public Guid Id { get; set; }
    public int Order { get; set; }
    public ItemType ItemType { get; set; }
    public string Title { get; set; }
    public string? ContentJson { get; set; }
    public ProjectDto? Project { get; set; }
    
    public Guid CourseId { get; set; }
}
﻿using FullStack101.Core.Extensions;
using Microsoft.AspNetCore.Http;

namespace FullStack101.Core.Dtos.User;

public class CreateUserDto
{
    public string FirstName { get; set; } = null!;
    public string LastName { get; set; } = null!;
    public string Email { get; set;  } = null!;
    public string UserName { get; set; } = null!;
    public string Password { get; set; } = null!;
    public IFormFile? Avatar { get; set; }
    public DateTime BirthDate { get; set; }

    public Domain.Entities.User ToUserEntity() => new()
    {
        FirstName = FirstName,
        LastName = LastName,
        Email = Email,
        UserName = UserName,
        BirthDate = BirthDate.SetKindUtc()
    };
}
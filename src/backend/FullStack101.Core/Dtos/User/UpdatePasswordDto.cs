﻿namespace FullStack101.Core.Dtos.User;

public class UpdatePasswordDto
{
    public string OldPassword { get; set; }
    public string NewPassword { get; set; }
    public string RetryPassword { get; set; }
}
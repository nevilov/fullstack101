﻿using Microsoft.AspNetCore.Http;

namespace FullStack101.Core.Dtos.User;

public class UpdateUserDto
{
    public IFormFile? Avatar { get; set; } = null!;
    public string FirstName { get; set; } = null!;
    public string LastName { get; set; } = null!;
    public string Email { get; set;  } = null!;
    public DateTime BirthDate { get; set; }
}
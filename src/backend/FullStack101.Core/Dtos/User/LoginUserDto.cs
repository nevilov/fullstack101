﻿namespace FullStack101.Core.Dtos.User;

public class LoginUserDto
{
    public string Login { get; set; } = null!;
    public string Password { get; set; } = null!;
}
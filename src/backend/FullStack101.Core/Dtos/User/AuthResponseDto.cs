﻿namespace FullStack101.Core.Dtos.User;

public class AuthResponseDto
{
    public string Token { get; set; }
    public UserDto User { get; set; }
    public bool IsAdmin { get; set; }
}
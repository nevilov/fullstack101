﻿namespace FullStack101.Core.Dtos.User;

public class UserDto
{
    public string FirstName { get; set; } = null!;
    public string LastName { get; set; } = null!;
    public string Email { get; set;  } = null!;
    public string UserName { get; set; } = null!;
    public string? AvatarUrl { get; set; }
    
    public DateTime BirthDate { get; set; }
}
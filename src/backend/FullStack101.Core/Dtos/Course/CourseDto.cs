﻿namespace FullStack101.Core.Dtos.Course;

public class CourseDto
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
    public string DescriptionJson { get; set; } = null!;
    public string Slug { get; set; } = null!;
    public string? ImageUrl { get; set; }
    public decimal? Price { get; set; }
    public string SlightDescription { get; set; } = null!;
    
    public IEnumerable<ShortCourseItemDto> Items { get; set; }
}

public record ShortCourseItemDto(Guid Id, string Title, int Order);
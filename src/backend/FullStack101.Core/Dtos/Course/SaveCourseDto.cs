﻿using Microsoft.AspNetCore.Http;

namespace FullStack101.Core.Dtos.Course;

public class SaveCourseDto
{
    public Guid Id { get; set; } = Guid.NewGuid();
    public string Name { get; set; } = null!;
    public string DescriptionJson { get; set; } = null!;
    public string Slug { get; set; } = null!;
    public decimal? Price { get; set; }
    public string? SlightDescription { get; set; } = null!;
    
    public IFormFile? CoverImageFile { get; set; }
    
    public Domain.Entities.Course ToEntity()
    {
        return new Domain.Entities.Course()
        {
            Id = Id,
            Name = Name,
            DescriptionJson = DescriptionJson,
            Slug = Slug,
            Price = Price,
            SlightDescription = SlightDescription
        };
    }
}
﻿using FullStack101.Domain.Entities;

namespace FullStack101.Core.Dtos.Project;

public class SaveProjectDto
{
    public Guid? Id { get; set; }
    public string Title { get; set; } = null!;
    public string Description { get; set; } = null!;
    public string JsonTree { get; set; } = null!;
    public Guid? ReferencedProjectId { get; set; } = null!;
    public string PlatformName { get; set; }
    public bool OccupyPort { get; set; } = true;
    
    public string Command { get; set; }
    public string Arguments { get; set; }
}
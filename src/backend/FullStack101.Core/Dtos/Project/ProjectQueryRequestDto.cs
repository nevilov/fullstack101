﻿namespace FullStack101.Core.Dtos.Project;

public class ProjectQueryRequestDto
{
    public bool FilterByUser { get; set; }
}
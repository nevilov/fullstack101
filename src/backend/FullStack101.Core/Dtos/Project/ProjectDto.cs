﻿using FullStack101.Domain.Entities;

namespace FullStack101.Core.Dtos.Project;

public class ProjectDto
{
    public Guid Id { get; set; }
    public string Title { get; set; } = null!;
    public string Description { get; set; } = null!;
    public string JsonTree { get; set; } = null!;
    public Platform Platform { get; set; } 
    public string Command { get; set; }
    public string Arguments { get; set; }
    public string? CreatedByUserName { get; set; }
}
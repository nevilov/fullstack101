﻿using System.Text.Json.Serialization;

namespace FullStack101.Core.Dtos.Project;

public class ProjectTreeDto
{
    [JsonPropertyName("name")]
    public string Name { get; set; }
    [JsonPropertyName("content")]
    public string? Content { get; set; }
    [JsonPropertyName("language")]
    public string? Language { get; set; }
    [JsonPropertyName("children")]
    public ProjectTreeDto[]? Children { get; set; }
}
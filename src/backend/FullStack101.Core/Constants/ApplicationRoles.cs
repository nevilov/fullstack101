﻿namespace FullStack101.Core.Constants;

public class ApplicationRoles
{
    public const string User = nameof(User);
    public const string Administrator = "Administrator";
}
﻿namespace FullStack101.Core.Constants;

public class ApplicationClaimTypes
{
    public const string Email = nameof(Email);
    public const string UserName = nameof(UserName);
    public const string Role = nameof(Role);
}

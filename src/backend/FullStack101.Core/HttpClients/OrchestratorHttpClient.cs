﻿using System.Net.Http.Json;
using FullStack101.Core.HttpClients.Dtos;

namespace FullStack101.Core.HttpClients;

public class OrchestratorHttpClient
{
    private readonly HttpClient _http;

    public OrchestratorHttpClient(HttpClient http)
    {
        _http = http;
    }

    public async Task RunContainer(CreateContainerRequestDto dto)
    {
        await _http.PostAsJsonAsync("/api/container", dto);
    }
}
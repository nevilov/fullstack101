﻿namespace FullStack101.Core.Abstractions.Helpers;

public interface ICurrentUserAccessor
{ 
    string GetUserName();
}
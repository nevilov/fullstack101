using FullStack101.Core.Dtos.S3;
using Microsoft.AspNetCore.Http;

namespace FullStack101.Core.Abstractions.Services
{
    public interface IS3Service
    {
        Task<string> CreateObject(IFormFile file, string fileName);
        Task CreateObject(MemoryStream stream, string key);
        Task CreateObject(byte[] byteArray, string key);
        Task<IList<string>> ListBucketContent(string prefix);
        Task CreateObject(S3ObjectRequest obj);

        string GetObjectUrl(string key);
    }
}
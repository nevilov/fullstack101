﻿using FullStack101.Domain.Entities;

namespace FullStack101.Core.Abstractions.Repositories;

public interface ICourseRepository
{
    Task SaveChanges();
    Task<IEnumerable<Course>> GetCourses();
    Task Add(Course course);
    Task<Course?> GetBySlug(string slug);
    Task<Course?> GetById(Guid id);
}
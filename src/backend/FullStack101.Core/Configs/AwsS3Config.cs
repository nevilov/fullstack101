namespace FullStack101.Core.Configs
{
    public class AwsS3Config
    {
        public string ServiceUrl { get; set; } = null!;
        public string BucketName { get; set; } = null!;
        public string CustomServiceUrl { get; set; } = null!;
    }
}
﻿namespace FullStack101.Core.Configs;

public class JwtConfig
{
    public string Issuer { get; set; } = null!;
    public string SecretKey { get; set; } = null!;
    public int ExpireInMinutes { get; set; }
}

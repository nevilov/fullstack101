﻿using System.Net;
using System.Text.Json;
using FullStack101.Api.Contracts;
using FullStack101.Core.Exceptions;
using ILogger = Serilog.ILogger;

namespace FullStack101.Api.Middlewares;

public class ExceptionHandler
{
    private readonly RequestDelegate _next;
    private readonly ILogger _logger;

    public ExceptionHandler(RequestDelegate next, ILogger logger)
    {
        _next = next;
        _logger = logger;
    }

    public async Task Invoke(HttpContext context)
    {
        try
        {
            await _next(context);
        }
        catch (Exception error)
        {
            _logger.Error(error, "An error occured");
            var response = context.Response;
            response.ContentType = "application/json";

            response.StatusCode = error switch
            {
                NotFoundException => (int)HttpStatusCode.NotFound,
                AuthException => (int)HttpStatusCode.Unauthorized,
                _ => (int)HttpStatusCode.InternalServerError
            };

            var result = JsonSerializer.Serialize(new ExceptionResponse
            {
                StatusCode = response.StatusCode,
                Message = error?.Message
            });
                
            await response.WriteAsync(result);
        }
    }
}
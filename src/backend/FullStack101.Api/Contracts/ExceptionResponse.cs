﻿namespace FullStack101.Api.Contracts;

public class ExceptionResponse
{
    public int StatusCode { get; set;  }
        
    public string? Message { get; set; }
}
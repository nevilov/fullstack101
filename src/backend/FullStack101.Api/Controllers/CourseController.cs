﻿using FullStack101.Core.Dtos.Course;
using FullStack101.Core.Dtos.CourseItem;
using FullStack101.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace FullStack101.Api.Controllers;

[ApiController]
[Route("api/[Controller]")]
public class CourseController : ControllerBase
{
    private readonly CourseService _courseService;

    public CourseController(CourseService courseService)
    {
        _courseService = courseService;
    }

    [HttpGet]
    public async Task<IActionResult> Get()
    {
        return Ok(await _courseService.Get());
    }

    [HttpGet("{slug}")]
    public async Task<IActionResult> GetBySlug([FromRoute] string slug)
    {
        return Ok(await _courseService.GetBySlug(slug));
    }

    [HttpPut]
    public async Task<IActionResult> Update([FromForm] SaveCourseDto dto)
    {
        await _courseService.Update(dto);
        return Ok();
    }
    
    [HttpPost]
    public async Task<IActionResult> Create([FromForm] SaveCourseDto dto)
    {
        await _courseService.Create(dto);
        return Ok();
    }

    [HttpPost("{id}/course-items/order")]
    public async Task<IActionResult> UpdateItemsOrder(Guid id, IEnumerable<ShortCourseItemDto> courseItemDtos)
    {
        await _courseService.UpdateItemsOrder(id, courseItemDtos);
        return Ok();
    }
}
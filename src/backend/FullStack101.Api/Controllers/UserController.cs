﻿using FullStack101.Core.Dtos.User;
using FullStack101.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;

namespace FullStack101.Api.Controllers;

[ApiController]
[Route("api/[Controller]")]
public class UserController : ControllerBase
{
    private readonly UserService _userService;

    public UserController(UserService userService)
    {
        _userService = userService;
    }

    [HttpPost("login")]
    public async Task<IActionResult> Login(LoginUserDto loginUserDto)
    {
        return Ok(await _userService.Login(loginUserDto));
    }

    [HttpPost("register")]
    [Produces("application/json")]
    public async Task<IActionResult> Register([FromForm] CreateUserDto userDto)
    {
        return Ok(await _userService.Register(userDto));
    }

    [HttpPut("profile")]
    [Authorize]
    public async Task<IActionResult> UpdateProfile([FromForm] UpdateUserDto dto)
    {
        await _userService.Update(dto);
        return Ok();
    }

    [HttpPut("password")]
    public async Task<IActionResult> UpdatePassword(UpdatePasswordDto dto)
    {
        await _userService.UpdatePassword(dto);
        return Ok();
    }

    [HttpGet("current")]
    [Authorize]
    public async Task<IActionResult> GetCurrentUser()
    {
        return Ok(await _userService.GetCurrentUser());
    }
}
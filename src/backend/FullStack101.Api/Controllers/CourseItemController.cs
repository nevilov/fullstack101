﻿using FullStack101.Core.Dtos.CourseItem;
using FullStack101.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace FullStack101.Api.Controllers;

[ApiController]
[Route("api/[Controller]")]
public class CourseItemController : ControllerBase
{
    private readonly CourseItemService _courseItemService;

    public CourseItemController(CourseItemService courseItemService)
    {
        _courseItemService = courseItemService;
    }

    [HttpPost]
    public async Task<IActionResult> Create([FromBody]SaveCourseItemDto dto)
    {
        await _courseItemService.Create(dto);
        return Ok();
    }

    [HttpGet("{id:Guid}")]
    public async Task<IActionResult> Get(Guid id)
    {
        return Ok(await _courseItemService.Get(id));
    }

    [HttpPut("{id:guid}")]
    public async Task<IActionResult> Update(Guid id, [FromBody]SaveCourseItemDto dto)
    {
        await _courseItemService.Update(id, dto);
        return Ok();
    }
}
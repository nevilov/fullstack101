﻿using FullStack101.Core.Dtos.Image;
using FullStack101.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FullStack101.Api.Controllers;

[ApiController]
[Route("api/[controller]")]
[Authorize]
public class ImageController(ImageService imageService) : ControllerBase
{
    [HttpPost]
    [Produces("application/json")]
    public async Task<ActionResult<string>> Upload([FromForm]ImageDto dto)
    {
        return Ok(await imageService.Upload(dto));
    }
}
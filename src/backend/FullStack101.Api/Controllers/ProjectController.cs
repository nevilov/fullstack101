﻿using FullStack101.Core.Dtos.Project;
using FullStack101.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FullStack101.Api.Controllers;

[ApiController]
[Route("api/[Controller]")]
[Authorize]
public class ProjectController : ControllerBase
{
    private readonly ProjectService _projectService;

    public ProjectController(ProjectService projectService)
    {
        _projectService = projectService;
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get([FromRoute] Guid id)
    {
        return Ok(await _projectService.GetProject(id));
    }
    
    [HttpGet]
    public async Task<IActionResult> Get([FromQuery]ProjectQueryRequestDto dto)
    {
        return Ok(await _projectService.Get(dto));
    }

    [HttpPost]
    public async Task<IActionResult> Save([FromBody]SaveProjectDto dto)
    {
        return Ok(await _projectService.Save(dto));
    }

    [HttpPost("Clone/{projectId}")]
    [Produces("application/json")]
    public async Task<IActionResult> Clone(Guid projectId)
    {
        return Ok(await _projectService.Clone(projectId));
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(Guid id)
    {
        await _projectService.Delete(id);
        return Ok();
    }

    [HttpPost("Run")]
    public async Task<IActionResult> Run(SaveProjectDto dto)
    {
        var projectId = await _projectService.Save(dto);
        await _projectService.Run(projectId);
        return Ok();
    }

    [HttpGet("platforms")]
    public async Task<IActionResult> GetPlatforms()
    {
        return Ok(await _projectService.GetPlatforms());
    }
}
using FullStack101.Api;
using FullStack101.Api.Middlewares;
using FullStack101.Core;
using FullStack101.Infrastructure;
using Serilog;
using Serilog.Exceptions;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog((hostingContext, loggerConfiguration) =>
{
    loggerConfiguration
        .Enrich.WithExceptionDetails()
        .ReadFrom.Configuration(hostingContext.Configuration)
        .WriteTo.Console();
});

builder.Services.AddControllers();
builder.Services.AddSwaggerGen();

builder.Services
    .AddHostModule(builder.Configuration)    
    .AddCoreModule(builder.Configuration)
    .AddInfrastructureModule(builder.Configuration);

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.UseCors(options =>
{
    options
        .WithOrigins(builder.Configuration["AppUrl"]!)
        .AllowAnyHeader()
        .AllowAnyMethod()
        .AllowCredentials();
});

app.UseMiddleware<ExceptionHandler>();

app.MapControllers();
app.Run();
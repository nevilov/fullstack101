# FullStack101 - Learn&Run
### Данная выпускная квалификационная работа является трудом и собственностью
### Студента гр. ПИН/б-20-1-о ИИТ СевГУ Юсуфова Алима Сейрановича

#DevInfo
---

## Fullstack101 - Backend

Create migration

``
dotnet ef migrations add "Init migrations" --project .\FullStack101.Infrastructure\ --startup-project .\FullStack101.Api\
``

Update db

``
dotnet ef database update --project .\FullStack101.Api\
``

---

---

## Setup logger sink
``
docker run --name seq -d -e ACCEPT_EULA=Y \
  -e SEQ_API_CANONICALURI=https://seq.fullstack101.ru \
  -e VIRTUAL_PORT=5341 \
  -e VIRTUAL_HOST=seq.fullstack101.ru \
  -e SEQ_FIRSTRUN_ADMINPASSWORDHASH="$PH" \
  -v /path/to/seq/data:/data \
  --network="nginx_default" \
  -p 80:80 \
  -p 5341:5341 \
  datalust/seq
``